## Principe général

*Version abrégée (sera complétée plus tard).*

![diagramme Lexika](documentation/diagramme%20lexika.svg "diagramme Lexika")

## Emploi actuel

Ce logiciel est le principal moteur de la [collection Lexica](https://benjamingalliot.gitlab.io/Lexica). *Lexi**k**a* fait référence à l'aspect technique, c'est-à-dire le logiciel, tandis que *Lexi**c**a* désigne le produit grand public de la **collection** du même nom, soit les dictionnaires conçus pour être humainement lisibles.

## Installation et utilisation

### Général

Ce programme est codé en [Julia](https://julialang.org) (version minimale 1.10). Il téléchargera si nécessaire [`saxon`](https://www.saxonica.com/download/java.xml), qui est utilisé pour les transformations [XSL](https://www.w3.org/TR/xslt/all) (version minimale 2), ainsi Java doit être installé sur votre système.
En revanche, [LaTeX](https://www.latex-project.org) (plus précisément [LuaLaTeX](https://www.luatex.org)), du fait de sa taille et de sa complexité, ne sera pas automatiquement téléchargé. Lexika tentera toutefois de trouver l’exécutable adéquat, sachant que l’utilisation de [`latexmk`](https://mg.readthedocs.io/latexmk.html) est fortement conseillée (et généralement inclus dans les distributions TeX). Il est crucial d'avoir une version très récente des modules LaTeX, aussi une version manuellement installée de votre distribution TeX sera peut-être nécessaire. Pour les utilisateurs Linux souhaitant installer une version à jour de [Texlive](https://www.tug.org/texlive), ce [lien](https://tex.stackexchange.com/questions/540429/tlmgr-in-ubuntu-20-04-local-tex-live-2019-is-older-than-remote-repository-2) pourrait être utile.

Interpréteur Julia : [https://julialang.org/downloads/](https://julialang.org/downloads)

Source (dépôt *git*) : [https://gitlab.com/BenjaminGalliot/JLexika](https://gitlab.com/BenjaminGalliot/JLexika)

### Installation

Étapes :
* installation de Julia :
  * installez l’[interpréteur](https://julialang.org/downloads) ;
  * ajoutez le chemin d’accès (jusqu’à *bin/*) à la variable *PATH* (par exemple avec une ligne dans le fichier *bashrc* ou équivalent : `export PATH="/home/[…]/julia/bin:$PATH"`) ;
* installation de Lexika :
  * clonez le dépôt du programme Lexika (par exemple avec `git clone https://gitlab.com/BenjaminGalliot/JLexika`) ;
  * installez le programme : `julia --project -e "import Pkg; Pkg.instantiate(); Pkg.precompile();"` ;
* utilisation de Lexika :
  * exécutez le programme : `julia --project exécuter.jl` ;
  * consultez les résultats dans le dossier *résultats*.

### Utilisation simple

* Pour lancer l’image : `docker run -it --rm -v *dossier_polices*:/home/lexika/.fonts -v *dossier_langue*:/home/lexika/dictionnaires/*langue* --name lexika lexika zsh` ;
  * `-v ~/.fonts:/home/lexika/.fonts` permet de lier votre dossier local des polices au dossier équivalent interne au conteneur (ici `~/.fonts` lié à `/home/lexika/.fonts`) (utile pour donner accès à des polices spécifiques) ;
  * `*dossier_langue*:/home/lexika/dictionnaires/*langue*` permet de lier votre dossier local de langue au dossier équivalent interne au conteneur (ici `~/.fonts` lié à `/home/lexika/.fonts`) (utile pour mettre à jour la source du dictionnaire depuis l’extérieur du conteneur) ;
* Pour créer un dictionnaire (depuis le conteneur) : `lexika *langue*` ;
* Pour récupérer les résultats (hors du conteneur mais sans l’avoir fermé) : `docker cp lexika:"/home/lexika/dictionnaires/*langue*" résultats`.


### Utilisation plus avancée

La commande princiale, décrite plus haut, et utilisable directement ou par un fichier exécuté, est la suivante :

```bash
julia --project exécuter.jl
```

Néanmoins, elle possède plusieurs arguments optionnels, que l’on peut voir avec l’argument `--help` :

```bash
julia --project exécuter.jl --help

Usage: julia --project exécuter.jl chemin/fichier/configuration.yml [--cible chemin/dossier/cible] [--html] [--latex]

Lance Lexika afin de créer un dictionnaire.

Positional Arguments:
chemin d’accès du fichier de configuration YAML
      Précise le dossier du fichier de configuration.
      (Type: String, Required)

Option Arguments:
--cible
      Précise le dossier cible des résultats.
      (Type: String, Default: ../résultats/)
--html
      Crée un dictionnaire final au format HTML.
      (Type: Flag)
--latex
      Crée un dictionnaire final au format LaTeX.
      (Type: Flag)
```

## Améliorations prévues et problèmes connus

* Les hyperliens des fichiers PDF ne fonctionnent pas sur certaines combinaisons de logiciels/navigateurs et systèmes d’exploitation, sans avoir pour le moment de trouver de schéma récurrent…
