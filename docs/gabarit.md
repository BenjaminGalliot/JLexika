# Documentation du module “module”

```@contents
Pages = ["“fichier”"]
```

## Index

```@meta
CurrentModule = Lexika.“module”
```

```@index
Modules = [“module”]
Order   = [:module, :constant, :type, :function, :macro]
```

## Functions

```@autodocs
Modules = [“module”]
Order   = [:function, :macro]
```
