<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:mode on-no-match="shallow-skip"/>

<xsl:template match="/">
    <xsl:apply-templates/>
    <xsl:text>&#xA;––––––––––––––––––––––––––––––––––––––––&#xA;</xsl:text>
    <xsl:call-template name="trouver_noms_scientifiques"/>
</xsl:template>

<!-- <xsl:template match="*[@langue='fra']/text()">
    <xsl:variable name="texte" select="."/>
    <xsl:variable name="texte_capitalisé" select="concat(upper-case(substring($texte, 1, 1)), substring($texte, 2))"/>
    <xsl:value-of select="concat($texte_capitalisé, if (not(matches($texte_capitalisé, '[.!?]$'))) then '.' else '')"/>
    <xsl:text>&#xA;</xsl:text>
</xsl:template> -->

<xsl:template match="EntréesLexicales">
    <xsl:for-each-group select=".//*[@langue='fra']/text()" group-by="concat(upper-case(substring(., 1, 1)), substring(., 2))">
        <xsl:variable name="ligne" select="current-grouping-key()"/>
        <xsl:variable name="ligne_traitée" select="concat($ligne, if (not(matches($ligne, '[.!?;:]$'))) then '.' else '')"/>
        <xsl:value-of select="$ligne_traitée"/>
        <xsl:text>&#xA;</xsl:text>
    </xsl:for-each-group>
</xsl:template>

<xsl:template name="trouver_noms_scientifiques">
    <xsl:for-each-group select=".//NomScientifique/Famille/text()" group-by=".">
        <xsl:sort select="."/>
        <xsl:variable name="ligne" select="current-grouping-key()"/>
        <xsl:value-of select="$ligne"/>
        <xsl:text>&#xA;</xsl:text>
    </xsl:for-each-group>
</xsl:template>


<xsl:template match="*">
    <xsl:apply-templates/>
</xsl:template>


</xsl:stylesheet>
