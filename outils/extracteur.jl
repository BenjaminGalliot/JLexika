using FilePaths
# using XML

# Exemple d’utilisation
# chemin_source::PosixPath = "dictionnaires/yuanga/yuanga.xml"
# chemin_cible::PosixPath = chemin_source
# balise_à_déplacer::String = "rf"
# balises_adelphes_prioritaires::Vector{String} = ["xv", "xe", "xf", "xn", "xc", "xo"]

chemin_source::PosixPath = "dictionnaires/yuanga/yuanga.xml"
chemin_cible::PosixPath = ""
balise_à_déplacer::String = ""
balises_adelphes_prioritaires::Vector{String} = ["", ""]

# document = read(chemin_source, LazyNode)

# for ligne ∈ first(document, 10)
#     ligne |> propertynames |> println
#     @show(ligne.raw)
#     @show(ligne.nodetype)
#     @show(ligne.tag)
#     @show(ligne.attributes)
#     @show(ligne.value)
#     @show(ligne.depth)
#     @show(ligne.children)
# end

chemin_style::PosixPath = "outils/extracteur.xsl"
chemin_cible::PosixPath = "yuanga texte fra.txt"

commande = `java -jar exécutables/saxon/saxon-he-12.4.jar -s:$chemin_source -xsl:$chemin_style -o:$chemin_cible`

run(commande)