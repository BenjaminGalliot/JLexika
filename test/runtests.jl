using Lexika
using Test

fichiers_tests = [
    "trieur.jl"]

for fichier_test ∈ fichiers_tests
    include(fichier_test)
end
