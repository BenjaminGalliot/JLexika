\directlua{pdf.setminorversion(7)}
\NeedsTeXFormat{LaTeX2e}
\RequirePackage{snapshot}
\RequirePackage{expl3}
\ProvidesExplClass{dictionnaire lexica}{2024/11/08}{0.5}{Classe de dictionnaire de la collection Lexica}
\cs_set:Nn \__fontspec_load_external_fontoptions:Nn {}

\RequirePackage{l3keys2e}
\RequirePackage[dvipsnames, table]{xcolor} % svgnames et x11names sont en RVB, peu adapté pour les impressions (il faut du CMJN).

% Gestion des variables principales de document.

\keys_define:nn {document} {
    type .tl_set:N = \l_document_type,
    type .default:n = book,
    type .initial:n = book,
}

% Paramétrage des langues et polices.
\input{langues}

\ProcessKeysOptions{document}
\ProcessKeysOptions{langues}

\DeclareOption*{\PassOptionsToClass{12pt}{\l_document_type}}
\ProcessOptions\relax
\LoadClass{\l_document_type}

% Configuration des langues et polices.
\input{configuration~linguistique}

% \RequirePackage{showframe}

\RequirePackage[a4paper, top=15mm, bottom=20mm, inner=15mm, outer=20mm]{geometry}
\RequirePackage{calc}
\RequirePackage{pdfpages}
\RequirePackage[nopatch=item]{microtype}
\RequirePackage{multicol}
\RequirePackage[hypertexnames=false, bookmarks=true, bookmarksnumbered=true, bookmarksopenlevel=5, bookmarksdepth=5, colorlinks=true, urlcolor=RoyalPurple, linkcolor=Cyan, citecolor=OliveGreen]{hyperref}
\hypersetup{pdfcreator={Benjamin~Galliot~\&~LuaLaTeX~–~Lexika}, pdfkeywords={dictionnaire,~lexica,~lexika}, pdfsubject={Dictionnaire~de~langue~rare}, pdftitle={Dictionnaire~Lexica}}
\RequirePackage[depth=5, numbered]{bookmark} % Pour permettre des trous hiérarchiques dans les signets.
\RequirePackage{hypcap} % Pour que les ancres des flottants soient en haut et non en bas.
\RequirePackage[bibstyle=biblatex-gl, citestyle=authoryear-comp, useprefix, maxcitenames=2, maxbibnames=30, autolang=other, language=auto, clearlang=false, backend=biber]{biblatex}
\RequirePackage{tocloft} % Argument titles nécessaire pour une utilisation conjointe avec fncychap.
\RequirePackage[explicit, nobottomtitles]{titlesec}
% \RequirePackage{titletoc}
\RequirePackage{clrdblpg}
\RequirePackage{ifoddpage}
\RequirePackage{metalogo}
% \RequirePackage[all]{nowidow} % Évite les lignes orphelines mais crée des espaces variables entre paragraphes (entrées)…
\RequirePackage[inline]{enumitem} % Attention avec Babel-French
\RequirePackage{tabularray}
\UseTblrLibrary{booktabs} % Tabularray importe lui-même le module Booktabs.
\RequirePackage{langsci-gb4e}
% \RequirePackage{etoc} % Problème avec titlesec et les niveaux supplémentaires…
\RequirePackage{xkeyval}
\RequirePackage{fancyhdr}
\RequirePackage{xurl}
\RequirePackage{tikz}
\RequirePackage{eso-pic}
\RequirePackage{xhfill}
\RequirePackage{varwidth}

\RenewDocumentCommand \UrlFont {} {\ttfamily\addfontfeature{FakeStretch=0.8}}
\urlstyle{same}

\newlength{\seuiltitrebasdepage}
\setlength{\seuiltitrebasdepage}{-\textheight} % Valeur négative pour simuler l’option bottomtitles (par défaut), confer commentaire lié plus haut.
\renewcommand{\bottomtitlespace}{\seuiltitrebasdepage}
\setlength{\emergencystretch}{3em}
\setlength{\columnsep}{1.5cm}
\newlength{\indentationvedette}
\setlength{\indentationvedette}{-1em}
\newlength{\taillemarge}
\setlength{\taillemarge}{0.7cm}
\setlength{\headheight}{15pt} % Demandé par fancyhdr.

\NewDocumentCommand \nombredor {} {1.618}
\NewDocumentCommand \inversenombredor {} {0.618}
\NewDocumentCommand \complémentinversenombredor {} {0.382}

\definecolor{grisé}{gray}{0.4}
\NewDocumentCommand \textegrisé { m } {\textcolor{grisé}{#1}}

\NewDocumentCommand \plettresstylisées { m } {{\addfontfeatures{StylisticSet=6, Ligatures={Required, Common, Contextual, Discretionary, Historic, TeX}}#1}} % Pour les scripts latins en EB Garamond 12.

% Configuration des styles d’en-têtes et pieds de page.
\input{styles}

% Configuration des couvertures.
\input{pages~spéciales}

% Configuration des entités linguistiques communes aux dictionnaires.
\input{entités}

\addbibresource{lexika.bib}

\AtBeginDocument {
    \pagestyle{plain}
    \RenewDocumentCommand \chaptermark { m } {\markboth{#1}{}}
    \RenewDocumentCommand \sectionmark { m } {\markright{#1}}
    \normalfont
    \scriptlatin
    \frenchspacing
}
