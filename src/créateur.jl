module Créateur

using ..Structures
using ..Trieur
using ..Générateur
using DataFrames
using DataStructures
using Dates
using FilePaths
using Pipe

export configurer, créer_dictionnaire, créer_fichiers_latex

"Fonction créant la configuration linguistique nécessaire à tous les traitements ultérieurs. Elle compile les expressions rationnelles, analyse syntaxiquement les arguments abstractionnels et lie les abstractions entre elles (ascendants potentiels et descendants)."
function configurer(chemin_accès_informations_linguistiques::SystemPath)::ConfigurationLinguistique
    global configuration = ConfigurationLinguistique(chemin_accès_informations_linguistiques)
    liste_abstractions = [abstraction for abstraction ∈ values(configuration.abstractions)]
    analyser_abstractions!.(liste_abstractions)
    analyser_abstractions_descendantes!.(liste_abstractions)
    créer_relations_parenté_abstraction!.(liste_abstractions)
    return configuration
end

function créer_dictionnaire(; taille_échantillon::Union{Nothing, Int}=nothing, date::AbstractString="")::SystemPath
    (abstraction_racine, entité_racine, entité_ascendante_bloc) = initialiser_dictionnaire()
    données_analysées = analyser_structure!(entité_racine, entité_ascendante_bloc; taille_échantillon)
    ajuster_noms_chemins!(taille_échantillon, date)
    générer_xml(configuration.généralités["informations"]["chemins"]["xml"], entité_racine, configuration.généralités["informations"]["chemins"]["xsd"], Dict(configuration.généralités["modèles"]["texte enrichi"] .=> configuration.généralités["substitutions"]["texte enrichi"]))
    générer_xsd(configuration.généralités["informations"]["chemins"]["xsd"], abstraction_racine, configuration.abstractions)
    dossier_résultat = parent(configuration.généralités["informations"]["chemins"]["xml"])
    afficher_analyse(données_analysées)
    afficher_statistiques(données_analysées)
    return dossier_résultat
end

function créer_fichiers_latex(; créer_pdf::Bool=true, forcer::Bool=false, effacer_fichiers_temporaires::Bool=true)::Union{Vector{<:SystemPath}, SystemPath}
    if (haskey(configuration.généralités["informations"]["chemins"], "audio") && haskey(configuration.généralités["informations"]["chemins"], "pdf"))
        liste_informations = [
            if !isnothing(chemin_audio)
                Dict(
                    "dossieraudio" => relpath(parent(chemin_audio), parent(chemin_pdf)),
                    "formataudio" => splitext(chemin_audio)[2][2:end]
                )
            else
                Dict()
            end
            for (chemin_audio, chemin_pdf) ∈ zip(configuration.généralités["informations"]["chemins"]["audio"], configuration.généralités["informations"]["chemins"]["pdf"])
        ]
    else
        liste_informations = [Dict() for _ ∈ configuration.généralités["informations"]["chemins"]["latex"]]
    end
    if haskey.(Ref(configuration.généralités["informations"]["chemins"]), ["latex", "xsl", "pdf"]) |> all
        Threads.@threads for (chemin_xsl, chemin_latex, chemin_pdf, informations) ∈ zip(configuration.généralités["informations"]["chemins"]["xsl"], configuration.généralités["informations"]["chemins"]["latex"], configuration.généralités["informations"]["chemins"]["pdf"], liste_informations) |> collect
            réussite_latex = générer_latex(configuration.généralités["informations"]["chemins"]["xml"], chemin_xsl, chemin_latex, informations)
            if réussite_latex && créer_pdf
                générer_pdf(chemin_latex, chemin_pdf; forcer)
            end
        end
        if effacer_fichiers_temporaires
            effacer_fichiers_temporaires_latex.(configuration.généralités["informations"]["chemins"]["pdf"])
        end
    else
        @error("Demande de création de dictionnaire LaTeX insatisfaite du fait d’informations manquantes.")
    end
    return configuration.généralités["informations"]["chemins"]["latex"]
end

"Fonction lisant le fichier source (au format MDF/Lex)."
function analyser_source!(tableau_données::AbstractDataFrame; taille_échantillon::Union{Nothing, Int}=nothing)::DataFrame
    open(configuration.généralités["informations"]["chemins"]["source"]) do fichier
        bloc = 0
        for (position, ligne) ∈ (enumerate ∘ eachline)(fichier)
            if ligne ≠ ""
                bilan = match(configuration.généralités["modèles"]["ligne"], ligne)
                if !isnothing(bilan)
                    bloc = (bilan[:balise] ∈ configuration.généralités["balises de bloc"]) ? bloc += 1 : bloc
                    push!(tableau_données, (
                        [position],
                        bloc,
                        bilan[:balise] |> String,
                        strip(bilan[:données]),
                        "métadonnées" ∈ keys(bilan) && !isnothing(bilan[:métadonnées]) ? Dict(bilan[:clef] => bilan[:valeur] for bilan ∈ eachmatch(configuration.généralités["modèles"]["métadonnées"], strip(bilan[:métadonnées]))) : Dict(),
                        [],
                        [],
                        [],
                        []))
                else
                    push!(tableau_données[end, :position], position)
                    tableau_données[end, :données] *= " " * strip(ligne)
                end
            end
            if !isnothing(taille_échantillon) && bloc == taille_échantillon
                break
            end
        end
    end
    return tableau_données
end

"Fonction analysant la structure du fichier source, appelant les autres fonctions clefs en séparant les différents blocs indépendants."
function analyser_structure!(entité_racine::Entité, entité_ascendante_bloc::Entité; taille_échantillon::Union{Nothing, Int}=nothing)::DataFrame
    tableau_données = DataFrame(
        position=Vector{Int}[],
        bloc=Int[],
        balise=Union{Balise, String}[],
        données=String[],
        métadonnées=Dict{String, String}[],
        abstractions=Vector{Abstraction}[],
        abstractions_ascendantes=Vector{Vector{Abstraction}}[],
        entités=Vector{Entité}[],
        entités_ascendantes=Vector{Vector{Entité}}[])
    analyser_source!(tableau_données; taille_échantillon)
    initialiser_élements!(tableau_données)
    analyser_éléments!(tableau_données, entité_racine, entité_ascendante_bloc)
    return tableau_données
end

"Fonction créant les premiers éléments ne souffrant d’aucun choix à faire."
function initialiser_élements!(tableau_données::AbstractDataFrame)::DataFrame
    balises_inconnues = Set{String}()
    for ligne ∈ eachrow(tableau_données)
        balise = trouver_balise(ligne.balise)
        if !ismissing(balise)
            informations = !ismissing(balise) ? répartir_informations_balise(ligne.position, balise, ligne.données, ligne.métadonnées) : nothing
            abstractions = !ismissing(balise) ? trouver_abstractions(balise) |> skipmissing |> collect : []
            entités = !ismissing(balise) ? [créer_entité(abstraction, valeur, caractéristiques, afficher_position(ligne.position)) for (abstraction, valeur, caractéristiques) ∈ zip(abstractions, informations.valeurs, informations.caractéristiques) if !isnothing(valeur)] : []
            ligne.balise = !ismissing(balise) ? balise : ligne.balise |> String
            ligne.abstractions = abstractions
            ligne.entités = entités
        else
            push!(balises_inconnues, ligne.balise)
        end
    end
    if !isempty(balises_inconnues)
        @info("Les balises suivantes n’ont pas été reconnues : $(join(sort(collect(balises_inconnues)), ", ", " et "))")
    end
    return tableau_données
end

"Fonction analysant les différents éléments en partitionnant les données en blocs indépendants (typiquement des entrées lexicales) puis en les joignant à l’entité ascendante générale."
function analyser_éléments!(tableau_données::AbstractDataFrame, entité_racine::Entité, entité_ascendante_bloc::Entité)::DataFrame
    liste_entités_totale::Vector{Entité} = []
    for bloc ∈ groupby(tableau_données, :bloc)
        liste_entités = vcat(bloc.entités...)
        résoudre_ascendances!(bloc, liste_entités, entité_ascendante_bloc)
        créer_identifiants!(liste_entités)
        append!(liste_entités_totale, liste_entités)
    end
    identifiants = inventorier_identifiants(liste_entités_totale)
    créer_liens!(tableau_données, identifiants)
    trier_entités!(entité_racine)
    return tableau_données
end

"Fonction créant les entités associées nécessitant des choix parfois complexes."
function résoudre_ascendances!(tableau_données::AbstractDataFrame, liste_entités::AbstractVector{Entité}, entité_ascendante_bloc::Entité)::DataFrame
    for ligne ∈ tableau_données |> eachrow
        for entité ∈ ligne.entités
            ascendance = créer_ascendance_adéquate!(liste_entités, entité_ascendante_bloc, entité, afficher_position(ligne.position))
            push!(ligne.entités_ascendantes, ascendance)
        end
        ligne.abstractions_ascendantes = [[entité.abstraction for entité ∈ entités] for entités ∈ ligne.entités_ascendantes]
    end
    return tableau_données
end

"Fonction trouvant les abstractions ascendantes adéquates d’une entité et crée le cas échéant les entités associées (avec leurs jonctions)."
function créer_ascendance_adéquate!(liste_entités::AbstractVector{Entité}, entité_ascendante_bloc::Entité, entité::Entité, position::AbstractString, succession_actuelle::AbstractVector{Entité}=Vector{Entité}())::Vector{Entité}
    déboguer("\n■ Entité/abstraction en cours : $entité/$(entité.abstraction)$(!isnothing(entité.valeur) ? " : $(entité.valeur)" : "")$(!isnothing(entité.caractéristiques) ? " – $(join(entité.caractéristiques, ", "))" : "")")
    if entité.abstraction.informations["arguments"]["bloc"]
        lier_entités!(entité_ascendante_bloc, entité)
        déboguer("→ Entité ascendante bloc liée : $entité avec $entité_ascendante_bloc")
    else
        ascendant = trouver_ascendant_adéquat(liste_entités, entité)
        if !isnothing(ascendant.erreur)
            @error("$(ascendant.erreur) avec l’entité/abstraction suivante : $entité/$(entité.abstraction)$(!isnothing(entité.valeur) ? " ($(entité.valeur))" : "")) à la ligne $position.")
        elseif !isnothing(ascendant.entité)
            lier_entités!(ascendant.entité, entité)
            déboguer("→ Entité ascendante liée : $entité avec $(ascendant.entité)")
        elseif !isnothing(ascendant.abstraction)
            # if ascendant.abstraction.informations["arguments"]["bloc"] && ascendant.abstraction ∈ [entité.abstraction for entité ∈ liste_entités]
            if ascendant.abstraction.informations["arguments"]["bloc"] && ascendant.abstraction == liste_entités[1].abstraction
                @error("Méstructuration de la source (abstraction ascendante $(ascendant.abstraction) choisie interdite car entité bloc associée $(liste_entités[1]) déjà créée) avec l’entité/abstraction suivante : $entité/$(entité.abstraction)$(!isnothing(entité.valeur) ? " ($(entité.valeur))" : "") à la ligne $position.")
            else
                entité_ascendante = créer_entité(ascendant.abstraction)
                entité_ascendante.origine = position
                pushfirst!(succession_actuelle, entité_ascendante)
                lier_entités!(entité_ascendante, entité)
                déboguer("→ Entité ascendante créée : $entité_ascendante (liée à $entité)")
                insert!(liste_entités, findfirst(isequal(entité), liste_entités), entité_ascendante)
                créer_ascendance_adéquate!(liste_entités, entité_ascendante_bloc, entité_ascendante, position, succession_actuelle)
            end
        else
            @error("Ni entité, ni abstraction pour $entité !")
        end
    end
    return succession_actuelle
end

"Fonction critique choisissant le meilleur ascendant parmi les candidats possibles, en utilisant toutes les règles à disposition (cardinalité, proximité, congruence, etc.)."
function trouver_ascendant_adéquat(liste_entités::AbstractVector{Entité}, entité::Entité)::NamedTuple{(:entité, :abstraction, :erreur), Tuple{Union{Entité, Nothing}, Union{Abstraction, Nothing}, Union{String, Nothing}}}
    abstractions_ascendantes_potentielles = (collect ∘ skipmissing)(trouver_abstractions_ascendantes(entité.abstraction))
    liste_abstractions = [entité.abstraction for entité ∈ liste_entités]
    entités_candidates::Vector{Entité} = []
    meilleure_entité_candidate::Union{Entité, Nothing} = nothing
    meilleure_abstraction_candidate::Union{Abstraction, Nothing} = nothing
    erreur = nothing
    déboguer("  Recherche ascendantale pour $entité/$(entité.abstraction)$(!isnothing(entité.valeur) ? " : $(entité.valeur)" : "") ")
    déboguer("  Abstractions potentielles à rechercher : $(join(abstractions_ascendantes_potentielles, ", "))")
    for (position_abstraction, abstraction_ascendante_potentielle) ∈ abstractions_ascendantes_potentielles |> enumerate
        déboguer("    Abstraction potentielle à rechercher : $position_abstraction/$(length(abstractions_ascendantes_potentielles)) – $(abstraction_ascendante_potentielle)")
        arguments = trouver_arguments_abstraction_descendante(abstraction_ascendante_potentielle, entité.abstraction.nom)
        entités_ascendantes_potentielles = liste_entités[findall(isequal(abstraction_ascendante_potentielle), liste_abstractions)]
        déboguer("    Entités potentielles à rechercher ($(length(entités_ascendantes_potentielles))) : $(join(entités_ascendantes_potentielles, ", "))")
        for (position_entité, entité_ascendante_potentielle) ∈ entités_ascendantes_potentielles |> enumerate
            déboguer("      Entité potentielle en cours d’analyse : $position_entité/$(length(entités_ascendantes_potentielles)) – $(entité_ascendante_potentielle)")
            if !isnothing(entité_ascendante_potentielle.descendants)
                if entité_ascendante_potentielle.origine == entité.origine
                    push!(entités_candidates, entité_ascendante_potentielle)
                    déboguer("      Candidat ajouté (car même origine) : $entité_ascendante_potentielle")
                elseif !entité_ascendante_potentielle.perceptible
                    déboguer("        Entité imperceptible car bannie.")
                elseif haskey(arguments, "cardinalité") && length([entité_adelphe for entité_adelphe ∈ entité_ascendante_potentielle.descendants if entité_adelphe.abstraction == entité.abstraction]) ≥ arguments["cardinalité"]
                    déboguer("        Cardinalité insatisfaite (au maximum $(arguments["cardinalité"]) $(entité.abstraction) par $abstraction_ascendante_potentielle).")
                elseif (ascendant_interdit = get(entité.abstraction.informations["arguments"], "interdiction ascendant", nothing)) |> !isnothing
                    ascendance = trouver_ascendance(entité_ascendante_potentielle)
                    if ascendant_interdit ∈ [entité.nom for entité ∈ ascendance]
                        entité_ascendante_potentielle.perceptible = false
                        for ascendant ∈ ascendance
                            ascendant.perceptible = false
                            if ascendant.abstraction.nom == ascendant_interdit
                                break
                            end
                        end
                        déboguer("        Autorisation ascendentale insatisfaite (présence de $ascendant_interdit) : bannissement de toute la lignée.")
                    end
                elseif !vérifier_congruence_ascendantale(entité_ascendante_potentielle, liste_entités, liste_abstractions)
                    déboguer("        Récence insatisfaite.")
                else
                    déboguer("      Candidat ajouté : $entité_ascendante_potentielle")
                    push!(entités_candidates, entité_ascendante_potentielle)
                end
            else
                erreur = "Aucun descendant possible pour $entité_ascendante_potentielle ($(entité_ascendante_potentielle.abstraction)) (problème de configuration des balises ou abstractions)"
            end
        end
    end
    déboguer("  Entités candidates : $(join(entités_candidates, ", "))")
    if length(entités_candidates) > 1
        meilleure_entité_candidate = liste_entités[maximum([findfirst(isequal(entité_candidate), liste_entités) for entité_candidate ∈ entités_candidates])]
        déboguer("  Entité choisie (parmi plusieurs) : $meilleure_entité_candidate")
    elseif length(entités_candidates) == 0
        if length(abstractions_ascendantes_potentielles) > 1  # À améliorer en analysant prospectivement les entités non encore analysées (dangereux et coûteux…).
            erreur = "Méstructuration de la source (ambigüité irrécupérable d’abstraction ascendante : $(join(abstractions_ascendantes_potentielles, ", ")))"  # Vérifier la position relative de la balise associée.
            meilleure_abstraction_candidate = nothing
        elseif length(abstractions_ascendantes_potentielles) == 0
            erreur = "Méparamétrage (aucune abstraction ascendante potentielle)"
            meilleure_abstraction_candidate = nothing
        else
            meilleure_abstraction_candidate = abstractions_ascendantes_potentielles |> only
            déboguer("  Abstraction choisie : $meilleure_abstraction_candidate")
        end
    else
        meilleure_entité_candidate = entités_candidates |> only
        déboguer("  Entité choisie : $meilleure_entité_candidate")
    end
    return (entité = meilleure_entité_candidate, abstraction = meilleure_abstraction_candidate, erreur = erreur)
end

"Fonction critique permettant de vérifier que toute la succession des abstractions ascendantes potentielles (avec polymorphisme géré) correspond bien aux entités les plus récentes de ces abstractions (parmi celles qui correspondent à certains critères paramétrables, comme la perceptibilité, le cas échéant), ainsi les incongruences ascendantales sont évitées (une entité quelconque ne se liera pas à un ascendant de même type si un autre candidat est plus récent)."
function vérifier_congruence_ascendantale(entité_ascendante_potentielle::Entité, liste_entités::AbstractVector{Entité}, liste_abstractions::AbstractVector{Abstraction})::Bool
    entités_ascendantes_potentielles = [entité_ascendante_potentielle]
    déboguer("        Analyse de congruence ascendentale pour $entité_ascendante_potentielle")
    while true
        déboguer("          Test de lignée : notre $entité_ascendante_potentielle $(entités_ascendantes_potentielles[end] ≠ entité_ascendante_potentielle ? "≠" : "=") $(entités_ascendantes_potentielles[end]) plus récente (parmi [$(join(entités_ascendantes_potentielles, ", "))])")
        if isempty(entités_ascendantes_potentielles) || entités_ascendantes_potentielles[end] ≠ entité_ascendante_potentielle
            déboguer("          Test échoué")
            return false
        elseif isnothing(entité_ascendante_potentielle) || entité_ascendante_potentielle.abstraction.informations["arguments"]["bloc"]
            déboguer("          Test arrêté (entité bloc)")
            break
        end
        abstractions_ascendantes_potentielles = (collect ∘ skipmissing)(trouver_abstractions_ascendantes(entité_ascendante_potentielle.abstraction))
        entités_ascendantes_potentielles = @pipe liste_entités[(sort ∘ vcat)([findall(isequal(abstraction_ascendante_potentielle), liste_abstractions) for abstraction_ascendante_potentielle ∈ abstractions_ascendantes_potentielles]...)] |> filter(entité -> entité.perceptible, _)
        entité_ascendante_potentielle = entité_ascendante_potentielle.ascendant
    end
    return true
end


"Fonction permettant de retrouver tous les ascendants d’une entité, utile notamment quand certains ascendants sont interdits."
function trouver_ascendance(entité::Entité, liste::AbstractVector{Entité}=Vector{Entité}())::Vector{Entité}
    if !entité.abstraction.informations["arguments"]["bloc"]
        push!(liste, entité.ascendant)
        trouver_ascendance(entité.ascendant, liste)
    end
    return liste
end

"Fonction créant les identifiants en rapatriant les identifiants des entités identifiées ascendantes."
function créer_identifiants!(liste_entités::AbstractVector{Entité})::Vector{Entité}
    informations::Dict{String, Union{String, Abstraction}} = Dict()
    for entité ∈ liste_entités
        if haskey(entité.abstraction.informations["arguments"], "identifiant")
            informations = Dict(
                "code" => entité.abstraction.informations["arguments"]["identifiant"]["code"],
                "abstraction" => entité.abstraction.informations["arguments"]["identifiant"]["abstraction"],
                "valeur" => entité.valeur)
            entité_identifiante = nothing
            identifiants_cumulés::Vector{String} = []
            entité_actuelle = entité.ascendant
            while !isnothing(entité_actuelle)
                if entité_actuelle.abstraction == informations["abstraction"]
                    entité_identifiante = entité_actuelle
                end
                if !isnothing(entité_actuelle.caractéristiques) && haskey(entité_actuelle.caractéristiques, "identifiant")
                    push!(identifiants_cumulés, entité_actuelle.caractéristiques["identifiant"])
                    break
                elseif entité_actuelle.abstraction.informations["arguments"]["bloc"]
                    break
                else
                    entité_actuelle = entité_actuelle.ascendant
                end
            end
            if !isnothing(entité_identifiante)
                if !isnothing(entité_identifiante) && !isnothing(entité_identifiante.caractéristiques)
                    entité_identifiante.caractéristiques["identifiant"] = join(identifiants_cumulés) * informations["code"] * informations["valeur"]
                else
                    entité_identifiante.caractéristiques = Dict("identifiant" => join(identifiants_cumulés) * informations["code"] * informations["valeur"])
                end
            end
        end
    end
    return liste_entités
end

"Fonction inventoriant les différents identifiants des entités et créant des versions simplifiées (utilisables dans le corps du dictionnaire) selon leur abstraction liée."
function inventorier_identifiants(liste_entités::AbstractVector{Entité}; séparateurs::AbstractVector{<:AbstractString}=["", " "])::Dict{String, Dict{Abstraction, String}}
    identifiants::Dict{String, Dict{Abstraction, String}} = Dict()
    abstractions_utiles = trouver_abstractions_cibles_liens()
    codes = trouver_abstractions_codes_identifiants()
    modèle_séparation = @pipe union(values(codes)...) |> join(_, "|") |> "(?<=$_)|(?=$_)" |> Regex
    for entité ∈ liste_entités
        if entité.abstraction ∈ abstractions_utiles && !isnothing(entité.caractéristiques) && haskey(entité.caractéristiques, "identifiant")
            identifiant_segmenté = split(entité.caractéristiques["identifiant"], modèle_séparation)
            identifiant_segmenté_simplifié::Vector{String} = []
            for (code, segment) ∈ zip(identifiant_segmenté[1:2:end], identifiant_segmenté[2:2:end])
                if code ∈ codes[entité.abstraction]
                    push!(identifiant_segmenté_simplifié, segment)
                end
            end
            for identifiant_simplifié ∈ join.(Ref(identifiant_segmenté_simplifié), séparateurs) |> Set
                identifiants_présents = get!(identifiants, identifiant_simplifié, Dict())
                nouvel_identifiant = entité.abstraction => entité.caractéristiques["identifiant"]
                if haskey(identifiants_présents, entité.abstraction)
                    @warn("Un même identifiant « $identifiant_simplifié » possède des cibles de même abstraction ($(entité.abstraction)) différentes : « $(identifiants_présents[entité.abstraction]) » et « $(entité.caractéristiques["identifiant"]) », le premier sera gardé le cas échéant.")
                end
                push!(identifiants_présents, nouvel_identifiant)
            end
        end
    end
    return identifiants
end

"Fonction liant les renvois aux cibles par les identifiants."
function créer_liens!(tableau_données::AbstractDataFrame, identifiants::AbstractDict{<:AbstractString, <:AbstractDict{Abstraction, <:AbstractString}})::DataFrame
    for entité ∈ vcat(tableau_données.entités...)
        if haskey(entité.abstraction.informations["arguments"], "cibles")
            informations_identifiant = get(identifiants, entité.valeur, missing)
            if !ismissing(informations_identifiant)
                for abstraction_cible_préférée ∈ entité.abstraction.informations["arguments"]["cibles"]
                    cible = get(informations_identifiant, abstraction_cible_préférée, missing)
                    if !ismissing(cible)
                        caractéristique = "identifiant" => cible
                        if isnothing(entité.caractéristiques)
                            entité.caractéristiques = Dict(caractéristique)
                        else
                            push!(entité.caractéristiques, caractéristique)
                        end
                        break
                    end
                end
            else
                @warn("L’identifiant « $(entité.valeur) » n’a pas trouvé de cible.")
            end
        end
    end
    return tableau_données
end

"Fonction triant les entités d’une arborescence."
function trier_entités!(entité_racine::Entité)::Entité
    trier_récursivement_entités!(entité_racine)
    return entité_racine
end

"Fonction triant les entités au sein d’une adelphie (par ordre entitique pour des entités d’abstractions différentes, puis par ordre lexicographique paramétrable pour des entités de même abstraction – pour le moment sur l’identifiant généré automatiquement)."
function trier_récursivement_entités!(entité_actuelle::Entité)
    if !isnothing(entité_actuelle.descendants) && !haskey(entité_actuelle.abstraction.informations["arguments"], "chemin")
        sort!(entité_actuelle.descendants, by = entité -> findfirst(isequal(entité.abstraction.nom), entité_actuelle.abstraction.informations["ordre des descendants"]))
        abstractions_entités_à_trier = [trouver_abstraction(descendant) for descendant ∈ entité_actuelle.abstraction.informations["ordre des descendants"]] |> skipmissing
        for abstraction_entité_à_trier ∈ abstractions_entités_à_trier
            if (critères_tri = get(trouver_arguments_abstraction_descendante(entité_actuelle.abstraction, abstraction_entité_à_trier.nom), "tri lexicographique", nothing)) |> !isnothing
                informations_lexicographiques = merge(configuration.généralités["ordre lexicographique"], configuration.généralités["informations"]["général"]["ordre lexicographique"] )
                ordre_lexicographique = get!(() -> créer_ordre_lexicographique(informations_lexicographiques, critères_tri), entité_actuelle.abstraction.informations, "ordre lexicographique")
                abstractions_descendants = [entité.abstraction for entité ∈ entité_actuelle.descendants]
                plage = findfirst(isequal(abstraction_entité_à_trier), abstractions_descendants):findlast(isequal(abstraction_entité_à_trier), abstractions_descendants)
                expressions_tri = [entité.caractéristiques["identifiant"] for entité ∈ entité_actuelle.descendants[plage]]
                valeurs_tri = Dict(expressions_tri .=> trier_entités.(expressions_tri, ordre_lexicographique))
                entité_actuelle.descendants[plage] = sort(entité_actuelle.descendants[plage], by = entité -> valeurs_tri[entité.caractéristiques["identifiant"]])
            end
        end
        trier_récursivement_entités!.(entité_actuelle.descendants)
    end
end

"Fonction analysant syntaxiquement les arguments abstractionnels, notamment pour préciser l’abstraction racine, l’abstraction bloc (qui correspond à l’unité du dictionnaire, etc.)"
function analyser_abstractions!(abstraction::Abstraction)::Abstraction
    arguments = analyser_arguments(abstraction)
    abstraction.informations["arguments"] = Dict{String, Any}(
        "racine" => [occursin("★", argument) for argument ∈ arguments] |> any,
        "bloc" => [occursin("¶", argument) for argument ∈ arguments] |> any,
        "texte enrichi" => [occursin("^", argument) for argument ∈ arguments] |> any
    )
    for argument ∈ arguments
        bilan_chemin = match(r"→(?<chemin>[\w\s’\/-]+)", argument)
        if !isnothing(bilan_chemin)
            abstraction.informations["arguments"]["chemin"] = bilan_chemin[:chemin]
        end
        bilan_identifiant = match(r"=(?<code>.+?)(?<abstraction>[\w\s’-]+)", argument)
        if !isnothing(bilan_identifiant)
            abstraction.informations["arguments"]["identifiant"] = Dict(
                "abstraction" => trouver_abstraction(bilan_identifiant[:abstraction]),
                "code" => bilan_identifiant[:code] |> String)
        end
        bilan_cibles = match(r"@\((?<cibles>[\w\s:,’-]+)\)", argument)
        if !isnothing(bilan_cibles)
            abstraction.informations["arguments"]["cibles"] = Vector{Abstraction}()
            for lien ∈ split(bilan_cibles[:cibles], ", ")
                abstractions_cibles = lien .|> trouver_abstraction
                push!(abstraction.informations["arguments"]["cibles"], abstractions_cibles)
            end
        end
        bilan_égalité_caractéristique = match(r"≡(?<caractéristique>[\w\s’-]+)", argument)
        if !isnothing(bilan_égalité_caractéristique)
            abstraction.informations["arguments"]["égalité caractéristique"] = bilan_égalité_caractéristique[:caractéristique]
        end
        bilan_interdiction_ascendant = match(r"❌(?<ascendant>[\w\s’-]+)", argument)
        if !isnothing(bilan_interdiction_ascendant)
            abstraction.informations["arguments"]["interdiction ascendant"] = bilan_interdiction_ascendant[:ascendant]
        end
    end
    return abstraction
end

"Fonction analysant les différents arguments potentiels d’une abstraction"
function analyser_arguments(abstraction::Abstraction; séparateur::Regex=r"\s*;\s*")::Vector
    arguments_bruts = get(abstraction.informations, "arguments", nothing)
    arguments = arguments_bruts |> !isnothing ? split(arguments_bruts, séparateur) : []
    return arguments
end

"Fonction analysant les abstractions descendantes, clarifiant leurs arguments et indiquant l’ordre des descendants."
function analyser_abstractions_descendantes!(abstraction::Abstraction)::Abstraction
    informations_abstractions_descendantes = get(abstraction.informations, "descendants", [])
    if !isnothing(informations_abstractions_descendantes)
        analyser_syntaxe_nom_abstraction_descendante!.(informations_abstractions_descendantes)
    end
    abstraction.informations["ordre des descendants"] = trouver_ordre_descendants(informations_abstractions_descendantes)
    return abstraction
end

function trouver_ordre_descendants(informations_abstractions_descendantes::AbstractVector)::Union{Vector{String}, Nothing}
    ordre_descendants = [informations_abstraction_descendante["nom"] for informations_abstraction_descendante ∈ informations_abstractions_descendantes]
    return length(ordre_descendants) > 0 ? ordre_descendants : nothing
end

"Fonction analysant syntaxiquement les arguments des abstractions descendantes, notamment pour préciser les descendants à créer impérativement, le nombre d’abstractions adelphes maximal, la structure des identifiants, les liens, etc."
function analyser_syntaxe_nom_abstraction_descendante!(informations_abstractions_descendantes::AbstractDict)::Dict
    modèle = configuration.généralités["modèles"]["nom d’abstraction"]
    bilan = match(modèle, informations_abstractions_descendantes["nom"])
    informations_abstractions_descendantes["nom"] = bilan[:nom] |> String
    informations_abstractions_descendantes["arguments"] = analyser_arguments_nom_abstraction(bilan[:arguments])
    return informations_abstractions_descendantes
end

function analyser_arguments_nom_abstraction(arguments::Union{SubString, Nothing})::Dict{String, Union{Bool, Int64, <:AbstractString}}
    arguments = !isnothing(arguments) ? arguments |> String : ""
    dictionnaire_arguments = Dict{String, Union{Bool, Int64, AbstractString}}()
    dictionnaire_arguments["impérieux"] = occursin("*", arguments)
    bilan_tri_lexicographique = match(r"<\((?<critères>[\w\d\s,:−+-]+)\)", arguments)
    if !isnothing(bilan_tri_lexicographique)
        dictionnaire_arguments["tri lexicographique"] = bilan_tri_lexicographique[:critères]
    end
    bilan_cardinalité = match(r"\[(?<nombre>[\d]+)\]", arguments)
    if !isnothing(bilan_cardinalité)
        dictionnaire_arguments["cardinalité"] = parse(Int, bilan_cardinalité[:nombre])
    end
    return dictionnaire_arguments
end

function créer_relations_parenté_abstraction!(abstraction::Abstraction)::Abstraction
    abstractions_descendantes = trouver_abstractions_descendantes(abstraction)
    ajouter_ascendant_au_descendant!.(abstraction, skipmissing(abstractions_descendantes))
    return abstraction
end

function ajouter_ascendant_au_descendant!(abstraction_ascendante::Abstraction, abstraction_descendante::Abstraction)::NamedTuple{(:abstraction_ascendante, :abstraction_descendante), Tuple{Abstraction, Abstraction}}
    if !haskey(abstraction_descendante.informations, "ascendants")
        abstraction_descendante.informations["ascendants"] = []
    end
    push!(abstraction_descendante.informations["ascendants"], Dict("nom" => abstraction_ascendante.nom))
    return (abstraction_ascendante=abstraction_ascendante, abstraction_descendante=abstraction_descendante)
end

"Fonction trouvant la balise associée à un nom. Si elle n’est pas trouvée, renvoie une valeur manquante accompagnée d’un avertissement."
function trouver_balise(nom_balise::Union{String, SubString})::Union{Balise, Missing}
    nom_balise = nom_balise |> String
    balise = get(configuration.balises, nom_balise, missing)
    if ismissing(balise)
        @warn("Balise $nom_balise non configurée.")
    end
    return balise
end

"Fonction trouvant les abstractions associées à une balise. Si elle n’est pas trouvée, renvoie une valeur manquante accompagné d’un avertissement."
function trouver_abstractions(balise::Balise)::Vector{Union{Abstraction, Missing}}
    return [trouver_abstraction(abstraction["nom"]) for abstraction ∈ balise.informations["abstractions"]]
end

"Fonction trouvant l’abstraction associée à un nom. Si elle n’est pas trouvée, renvoie une valeur manquante accompagnée d’un avertissement."
function trouver_abstraction(nom_abstraction::AbstractString)::Union{Abstraction, Missing}
    abstraction = get(configuration.abstractions, nom_abstraction, missing)
    if ismissing(abstraction)
        @warn("Abstraction $nom_abstraction non configurée.")
    end
    return abstraction
end

"Fonction trouvant les abstractions descendantes d’une abstraction. Si des descendants ne sont pas trouvés, des valeurs manquantes sont aussi renvoyées."
function trouver_abstractions_descendantes(abstraction::Abstraction)::Vector{Union{Abstraction, Missing}}
    return [trouver_abstraction(informations_abstraction["nom"]) for informations_abstraction ∈ get(abstraction.informations, "descendants", [])]
end

"Fonction trouvant les abstractions ascendantes (potentielles) d’une abstraction. Si des ascendants ne sont pas trouvés, des valeurs manquantes sont aussi renvoyées."
function trouver_abstractions_ascendantes(abstraction::Abstraction)::Vector{Union{Abstraction, Missing}}
    return [trouver_abstraction(informations_abstraction["nom"]) for informations_abstraction ∈ get(abstraction.informations, "ascendants", [])]
end

"Fonction trouvant les informations d’une abstraction descendante particulière."
function trouver_arguments_abstraction_descendante(abstraction::Abstraction, nom_descendant::AbstractString)::Union{Dict, Nothing}
    résultats = [informations_descendant["arguments"] for informations_descendant ∈ get(abstraction.informations, "descendants", []) if informations_descendant["nom"] == nom_descendant]
    return !isempty(résultats) ? résultats[1] : nothing
end

"Fonction trouvant les abstractions comportant des codes pour identifiants"
function trouver_abstractions_codes_identifiants()::Dict{Abstraction, Set{String}}
    codes::Dict{Abstraction, Set{String}} = Dict()
    for abstraction ∈ configuration.abstractions |> values
        if haskey(abstraction.informations["arguments"], "identifiant")
            push!(get!(codes, abstraction.informations["arguments"]["identifiant"]["abstraction"], Set()), abstraction.informations["arguments"]["identifiant"]["code"])
        end
    end
    return codes
end

"Fonction trouvant les abstractions cibles utiles pour les liens (notamment afin d’éviter de chercher des liens communs et peu pertinents, comme les groupes, les sens, etc.)"
function trouver_abstractions_cibles_liens()::Set{Abstraction}
    cibles::Set{Abstraction} = Set()
    for abstraction ∈ configuration.abstractions |> values
        if haskey(abstraction.informations["arguments"], "cibles")
            union!(cibles, abstraction.informations["arguments"]["cibles"] |> Set)
        end
    end
    return cibles
end

"Fonction créant l’entité à partir de son abstraction. Tous les arguments destinés à l’entité sont optionnels."
function créer_entité(abstraction::Abstraction, arguments...)::Union{Entité, Missing}
    entité = Entité(abstraction.informations["entité"]["nom"], abstraction, arguments...)
    # println("  * Entité créée : $entité ($(entité.abstraction))")
    return entité
end

# Fonctions en relation avec les données du dictionnaire.

"Fonction initialisant le dictionnaire en créant les premières entités impérieuses à partir de l’abstraction/entité racine, qui doit être unique."
function initialiser_dictionnaire()::NamedTuple{(:abstraction_racine, :entité_racine, :entité_ascendante_bloc), Tuple{Abstraction, Entité, Entité}}
    abstractions_racines = [abstraction for abstraction ∈ values(configuration.abstractions) if abstraction.informations["arguments"]["racine"]]
    if length(abstractions_racines) ≠ 1
        error("Une seule et unique abstraction doit être considérée comme une racine ($(join(abstractions_racines, ", ", " et "))) !")
    else
        abstraction_racine = abstractions_racines |> only
        entité_racine = abstraction_racine |> créer_descendance_entités
        entité_ascendante_bloc = trouver_entité_ascendante_bloc(entité_racine)
        return (abstraction_racine = abstraction_racine, entité_racine = entité_racine, entité_ascendante_bloc = entité_ascendante_bloc)
    end
end

function trouver_entité_ascendante_bloc(entité::Entité)::Entité
    for entité ∈ récupérer_entités_descendantes(entité)
        if haskey(entité.abstraction.informations, "descendants")
            for descendant ∈ entité.abstraction.informations["descendants"]
                abstraction = trouver_abstraction(descendant["nom"])
                if !ismissing(abstraction) && abstraction.informations["arguments"]["bloc"]
                    return entité
                end
            end
        end
    end
end

"Fonction répartissant les informations associées à une balise selon leur type (valeur ou caractéristique) et les abstractions liées. Différents traitements peuvent avoir lieu, tout comme la gestion des potentielles métadonnées (dont certaines sont à répartir entre différentes abstractions sans les dupliquer)."
function répartir_informations_balise(position::Vector{Int}, balise::Balise, données::AbstractString, métadonnées::Dict{<:AbstractString, <:AbstractString}=Dict())::NamedTuple{(:valeurs, :caractéristiques), Tuple{Vector{Union{String, Nothing}}, Vector{Union{Dict, Nothing}}}}
    informations = DataFrame(
        nom = String[],
        valeur = Union{String, Nothing}[],
        caractéristiques = Union{Dict, Nothing}[]
    )
    métadonnées_inutilisées = métadonnées |> keys |> collect
    for informations_abstraction ∈ balise.informations["abstractions"]
        nom = informations_abstraction["nom"]
        valeur = nothing
        caractéristiques = nothing
        if haskey(informations_abstraction, "traitement")
            if startswith(informations_abstraction["traitement"], "caractères superflus")
                bilan_caractères_superflus = match(r"caractères superflus : \[(?<caractères>.+)\]", informations_abstraction["traitement"])
                if !isnothing(bilan_caractères_superflus)
                    valeur = replace(données |> String, Dict(caractère => "" for caractère ∈ bilan_caractères_superflus[:caractères])...)
                end
            elseif startswith(informations_abstraction["traitement"], "métadonnée")
                bilan_métadonnées = match(r"métadonnée : (?<abstraction>.+)$", informations_abstraction["traitement"])
                nom_abstraction_cible = bilan_métadonnées[:abstraction]
                if haskey(informations_abstraction, "champ")
                    valeur = get(métadonnées, informations_abstraction["champ"], get(informations_abstraction, "valeur par défaut", nothing))
                    filter!(valeur -> valeur != informations_abstraction["champ"], métadonnées_inutilisées)
                    @debug("La métadonnée « $(informations_abstraction["champ"]) = $valeur » (balise $balise, position $(afficher_position(position))) deviendra une abstraction (en présence d’une valeur).")
                    for ligne in informations |> eachrow
                        if !isnothing(ligne.caractéristiques) && ligne.nom == nom_abstraction_cible
                            caractéristique_supprimée = pop!(ligne.caractéristiques, informations_abstraction["champ"], nothing)
                            if !isnothing(caractéristique_supprimée)
                                @debug("La métadonnée « $(informations_abstraction["champ"]) = $valeur » (balise $balise, position $(afficher_position(position))) devant devenir une abstraction, la caractéristique homonyme associée à l’abstraction « $(ligne.nom) » a été supprimée.")
                            end
                        end
                    end
                else
                    @warn("Le traitement « $(informations_abstraction["traitement"]) » (balise $balise, position $(afficher_position(position))) de l’abstraction « $(informations_abstraction["nom"]) » nécessite un champ correspondant et éventuellement une valeur par défaut (qui peut être nulle et le sera ici).")
                end
            else
                error("Traitement d’abstraction « $(informations_abstraction["traitement"]) » (balise $balise, position $(afficher_position(position))) inconnu.")
            end
        elseif haskey(informations_abstraction, "valeur")
            valeur = informations_abstraction["valeur"]
        else
            valeur = données |> String
        end
        informations_caractéristiques = get(informations_abstraction, "caractéristiques", nothing)
        if !isnothing(informations_caractéristiques)
            caractéristiques = OrderedDict()
            for informations_caractéristique ∈ informations_caractéristiques
                if haskey(informations_caractéristique, "traitement")
                    if informations_caractéristique["traitement"] == "code linguistique"
                        if haskey(configuration.généralités["informations"]["langues"], informations_caractéristique["valeur"])
                            push!(caractéristiques, informations_caractéristique["nom"] => configuration.généralités["informations"]["langues"][informations_caractéristique["valeur"]]["code"])
                            filter!(valeur -> valeur != informations_caractéristique["nom"], métadonnées_inutilisées)
                        else
                            error("La langue $(informations_caractéristique["valeur"]) de la balise $(balise.nom) n’est pas configurée.")
                        end
                    elseif informations_caractéristique["traitement"] == "métadonnée"
                        if haskey(informations_caractéristique, "champ")
                            valeur_caractéristique = get(métadonnées, informations_caractéristique["champ"], get(informations_caractéristique, "valeur par défaut", nothing))
                            if !isnothing(valeur_caractéristique)
                                push!(caractéristiques, informations_caractéristique["nom"] => valeur_caractéristique)
                                filter!(valeur -> valeur != informations_caractéristique["champ"], métadonnées_inutilisées)
                            end
                            if informations_caractéristique["champ"] == get(informations_abstraction, "champ", nothing)
                                @warn("Le traitement « $(informations_caractéristique["traitement"]) » (balise $balise, position $(afficher_position(position))) de l’abstraction « $(informations_abstraction["nom"]) » utilise un champ « $(informations_caractéristique["champ"]) » déjà utilisé par l’abstraction elle-même (répétition d’information).")
                            end
                        else
                            @warn("Le traitement « $(informations_caractéristique["traitement"]) » (balise $balise, position $(afficher_position(position))) de l’abstraction « $(informations_abstraction["nom"]) » nécessite un champ correspondant à la métadonnée associée à la caractéristique « $(informations_caractéristique["nom"]) » et éventuellement une valeur par défaut (qui peut être nulle et le sera ici).")
                        end
                    else
                        error("Traitement de caractéristique « $(informations_caractéristique["traitement"]) » (balise $balise, position $(afficher_position(position))) inconnu.")
                    end
                elseif haskey(informations_caractéristique, "valeur")
                    push!(caractéristiques, informations_caractéristique["nom"] => informations_caractéristique["valeur"])
                else
                    error("Caractéristique $informations_caractéristique (balise $balise) mal formée.")
                end
                caractéristiques_incompatibles = Dict(clef => (métadonnées[clef], caractéristiques[clef]) for clef in intersect(keys(métadonnées), keys(caractéristiques)) if métadonnées[clef] != caractéristiques[clef])
                if !isempty(caractéristiques_incompatibles)
                    for (nom_caractéristique, valeurs_caractéristique) ∈ caractéristiques_incompatibles
                        push!(caractéristiques, nom_caractéristique => valeurs_caractéristique[1])
                        @error("Il existe des caractéristiques incompatibles : « $nom_caractéristique » a les valeurs suivantes : « $(valeurs_caractéristique[1]) » (métadonnée) ≠ « $(valeurs_caractéristique[2]) » (caractéristique de la configuration) à la ligne $(afficher_position(position)) pour l’abstraction « $(informations_abstraction["nom"]) ». La métadonnée a eu la priorité.")
                    end
                end
            end
        end
        push!(informations, (nom, valeur, caractéristiques))
    end
    if !isempty(métadonnées_inutilisées)
        métadonnées_restantes = Dict(nom => valeur for (nom, valeur) ∈ métadonnées if nom ∈ métadonnées_inutilisées)
        @. informations[!, :caractéristiques] = merge!(ifelse(!isnothing(informations.caractéristiques), informations.caractéristiques, Dict()), [métadonnées_restantes])
    end
    return (valeurs=informations.valeur, caractéristiques=informations.caractéristiques)
end

"Fonction créant une descendance d’entités (se limitant aux descendants impérieux)."
function créer_descendance_entités(abstraction::Abstraction)::Entité
    entité_ascendante = créer_entité(abstraction)
    déboguer("→ Entité ascendante créée : $entité_ascendante")
    if (chemin = get(abstraction.informations["arguments"], "chemin", nothing)) |> !isnothing
        informations = accéder_informations(chemin)
        créer_entité_brute(entité_ascendante, informations; direct=true)
    end
    informations_abstractions_descendantes = get(abstraction.informations, "descendants", nothing)
    if !isnothing(informations_abstractions_descendantes)
        noms_abstractions_descendantes = [informations_abstraction["nom"] for informations_abstraction ∈ informations_abstractions_descendantes if informations_abstraction["arguments"]["impérieux"]]
        abstractions_descendantes = [abstraction for abstraction ∈ skipmissing(trouver_abstraction.(noms_abstractions_descendantes))]
        entités_descendantes = créer_descendance_entités.(abstractions_descendantes)
        lier_entités!.(Ref(entité_ascendante), entités_descendantes)
        déboguer("→ Entités descendantes liées : $(join(entités_descendantes, ", ")) (à $entité_ascendante)")
    end
    return entité_ascendante
end

"Fonction créant une entité brute, typiquement une information brute issue du fichier de configuration."
function créer_entité_brute(entité_ascendante::EntitéAbstraite, information::AbstractString; nom::AbstractString="élément", caractéristiques::Union{AbstractDict, Nothing}=nothing, direct::Bool=false)
    if direct
       entité_ascendante.valeur = information
    else
        entité_brute = EntitéBrute(; nom, caractéristiques, valeur=information, ascendant=entité_ascendante)
        lier_entités!(entité_ascendante, entité_brute)
    end
    return entité_ascendante
end

function créer_entité_brute(entité_ascendante::EntitéAbstraite, information::AbstractVector; nom::AbstractString="élément", caractéristiques::Union{AbstractDict, Nothing}=nothing, direct::Bool=false)
    entité_brute = if direct
        entité_ascendante
    else
        entité_brute = EntitéBrute(; nom, caractéristiques, ascendant=entité_ascendante)
        lier_entités!.(Ref(entité_ascendante), entité_brute)
        entité_brute
    end
    créer_entité_brute.(entité_brute, information)
end

function créer_entité_brute(entité_ascendante::EntitéAbstraite, information::AbstractDict; nom::AbstractString="élément", direct::Bool=false)
    abstraction = trouver_abstraction(information |> collect |> first |> first)
    if !ismissing(abstraction)
        caractéristiques = if haskey(abstraction.informations, "caractéristiques")
            champs = get.(abstraction.informations["caractéristiques"], "champ", nothing)
            OrderedDict(champ => pop!(information, champ) for champ ∈ champs)
        else
            nothing
        end
        for paire ∈ information
            créer_entité_brute(entité_ascendante, paire.second; nom=paire.first, caractéristiques)
        end
    elseif direct
        for paire ∈ information
            créer_entité_brute(entité_ascendante, paire.second; nom=paire.first)
        end
    else
        entité_brute = EntitéBrute(; nom, ascendant=entité_ascendante)
        lier_entités!.(Ref(entité_ascendante), entité_brute)
        for paire ∈ information
            créer_entité_brute(entité_brute, paire.second; nom=paire.first)
        end
    end
end

function créer_entité_brute(entité_ascendante::EntitéAbstraite, information)::EntitéAbstraite
    @error("Information ", information, " de type ($(typeof(information)) inconnu.")
    return entité_ascendante
end

"Fonction accédant aux informations notamment en vue de les insérer directement dans le dictionnaire."
function accéder_informations(chemin_accès::AbstractString)
    position = configuration.généralités["informations"]
    for sous_chemin ∈ split(chemin_accès, "/")
        position = position[sous_chemin]
    end
    return position
end

"Fonction liant deux entités, s’ajoutant comme descendant et ascendant."
function lier_entités!(entité_ascendante::EntitéAbstraite, entité_descendante::EntitéAbstraite)::NamedTuple
    entité_descendante.ascendant = entité_ascendante
    if isnothing(entité_ascendante.descendants)
        entité_ascendante.descendants = [entité_descendante]
    else
        push!(entité_ascendante.descendants, entité_descendante)
    end
    return (entité_ascendante=entité_ascendante, entité_descendante=entité_descendante)
end

"Fonction récupérant tous les descendants d’une entité."
function récupérer_entités_descendantes(entité::Entité, entités::AbstractVector{Entité}=Vector{Entité}())::Vector{Entité}
    if !isnothing(entité.descendants)
        for (index, descendant) ∈ enumerate(entité.descendants)
            if descendant isa Entité
                push!(entités, descendant)
                récupérer_entités_descendantes(descendant, entités)
            end
        end
    end
    return entités
end

function ajuster_noms_chemins!(taille_échantillon::Union{Nothing, Int}=nothing, date::AbstractString="", marqueur::AbstractString="test")
    if !isnothing(taille_échantillon) || !isempty(date)
        chemin = configuration.généralités["informations"]["chemins"]["xml"]
        nouveau_chemin = "$(splitext(chemin)[1]) $marqueur$(splitext(chemin)[2])"|> Path
        configuration.généralités["informations"]["chemins"]["xml"] = nouveau_chemin
        for type_fichier ∈ ["latex", "pdf"]
            chemins = configuration.généralités["informations"]["chemins"][type_fichier]
            nouveaux_chemins = ["$(splitext(chemin)[1]) $marqueur$(splitext(chemin)[2])" for chemin ∈ chemins] .|> Path
            configuration.généralités["informations"]["chemins"][type_fichier] = nouveaux_chemins
        end
    end
    if !isnothing(taille_échantillon)
        chemin = configuration.généralités["informations"]["chemins"]["xml"]
        nouveau_chemin = "$(splitext(chemin)[1]) $taille_échantillon$(splitext(chemin)[2])"|> Path
        configuration.généralités["informations"]["chemins"]["xml"] = nouveau_chemin
        for type_fichier ∈ ["latex", "pdf"]
            chemins = configuration.généralités["informations"]["chemins"][type_fichier]
            nouveaux_chemins = ["$(splitext(chemin)[1]) $taille_échantillon$(splitext(chemin)[2])" for chemin ∈ chemins] .|> Path
            configuration.généralités["informations"]["chemins"][type_fichier] = nouveaux_chemins
        end
    end
    if !isempty(date)
        date_actuelle = Dates.format(now(), date)
        chemin = configuration.généralités["informations"]["chemins"]["xml"]
        nouveau_chemin = "$(splitext(chemin)[1]) $date_actuelle$(splitext(chemin)[2])"|> Path
        configuration.généralités["informations"]["chemins"]["xml"] = nouveau_chemin
        for type_fichier ∈ ["latex", "pdf"]
            chemins = configuration.généralités["informations"]["chemins"][type_fichier]
            nouveaux_chemins = ["$(splitext(chemin)[1]) $date_actuelle$(splitext(chemin)[2])" for chemin ∈ chemins] .|> Path
            configuration.généralités["informations"]["chemins"][type_fichier] = nouveaux_chemins
        end
    end
end

function afficher_analyse(tableau_données::DataFrame)
    @info(tableau_données[1:5,[:balise, :abstractions, :abstractions_ascendantes, :entités, :entités_ascendantes]])
end

function afficher_position(position::AbstractVector{Int})
    if length(position) == 1
        résultat = "$(position[1])"
    elseif length(position) == 2
        résultat = "$(position[1]), $(position[end])"
    else
        résultat = "$(position[1])–$(position[end])"
    end
end

function afficher_statistiques(tableau_données::AbstractDataFrame)
    liste_balises_inconnues = @pipe Set(tableau_données.balise) |> collect |> filter(balise -> balise isa AbstractString, _)
    @info("Liste des balises inconnues : $(join(sort(liste_balises_inconnues), ", "))")
    liste_balises_analysées = @pipe Set(tableau_données.balise) |> collect |> filter(balise -> balise isa Balise, _)
    @info("Liste des balises analysées : $(join(sort(liste_balises_analysées, by = balise -> balise.nom), ", "))")
    liste_balises_inanalysées = setdiff(Set(configuration.balises |> values), Set(tableau_données.balise)) |> collect
    @info("Liste des balises inanalysées : $(join(sort(liste_balises_inanalysées, by = balise -> balise.nom), ", "))")
    liste_abstractions_analysées = Set()
    liste_entités_créées = String[]
    for ligne ∈ eachrow(tableau_données)
        valeurs_abstractions = vcat(ligne.abstractions, ligne.abstractions_ascendantes...)
        valeurs_entités = [entité.nom for entité ∈ vcat(ligne.entités, ligne.entités_ascendantes...)]
        push!.(Ref(liste_abstractions_analysées), valeurs_abstractions)
        push!.(Ref(liste_entités_créées), valeurs_entités)
    end
    compteur_entités = counter(liste_entités_créées)
    @info("Liste des abstractions analysées : $(join(sort(collect(liste_abstractions_analysées), by = abstraction -> abstraction.nom), ", "))")
    liste_abstractions_inanalysées = setdiff(Set(configuration.abstractions |> values), liste_abstractions_analysées) |> collect
    @info("Liste des abstractions inanalysées : $(join(sort(liste_abstractions_inanalysées, by = abstraction -> abstraction.nom), ", "))")
    @info("Liste des entités créées (et occurrences) : $(join(["$entité ($(compteur_entités[entité]))" for (entité, nombre_occurrence) in compteur_entités |> collect |> sort], ", "))")
end

end
