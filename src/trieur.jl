"Module regroupant tous les éléments nécessaires pour permettre le tri lexicographique."
module Trieur

using DataStructures
using Parameters
using Pipe

export OrdreLexicographique
export créer_ordre_lexicographique, trier_entités

@with_kw struct NiveauOrdreLexicographique
    nom::String
    modèle::Regex
    valeurs_tri::OrderedDict
    remplacements::Dict = Dict()
end

@with_kw struct CritèreTri
    niveau::NiveauOrdreLexicographique
    profondeur::Union{Int, Nothing} = nothing
    sens::Int = 1
end

@with_kw struct OrdreLexicographique
    critères::Vector{CritèreTri}
    remplacements::Dict
end

Base.broadcastable(ordre::OrdreLexicographique) = Ref(ordre)

function créer_ordre_lexicographique(informations_lexicographiques::AbstractDict, critères_lexicographiques::AbstractString; casse::Union{Symbol, Nothing}=nothing)::OrdreLexicographique
    niveaux = Dict{String, NiveauOrdreLexicographique}()
    niveaux["graphèmes"] = créer_ordre_graphémique(informations_lexicographiques["graphèmes"]; casse)
    for (nom, graphèmes) ∈ [information for information ∈ informations_lexicographiques if information[1] ≠ "graphèmes"]
        niveaux[nom] = créer_ordre_spécial(nom, graphèmes .|> string, niveaux["graphèmes"].modèle)
    end
    for (nom, niveau) ∈ niveaux
        println("niveau d’ordre lexicographique « $nom » : \n$(niveau.modèle) \n$(niveau.valeurs_tri)")
    end
    critères = analyser_critères(critères_lexicographiques, niveaux)
    ordre = OrdreLexicographique(;
        critères,
        remplacements = niveaux["graphèmes"].remplacements
    )
    return ordre
end

function analyser_critères(expression_critères::AbstractString, niveaux::Dict{<:AbstractString, NiveauOrdreLexicographique}; modèle_séparation::Regex=r"[\s,;–—]+", modèle_critère::Regex=r"^(?<niveau>\w+)(:(?<profondeur>\d+)?(?<sens>\-|−|\+)?)?$")::Vector{CritèreTri}
    critères = CritèreTri[]
    for expression_critère ∈ split(expression_critères, modèle_séparation)
        bilan = match(modèle_critère, expression_critère)
        if !isnothing(bilan)
            if haskey(niveaux, bilan[:niveau])
                critère = CritèreTri(
                    niveau = niveaux[bilan[:niveau]],
                    profondeur = !isnothing(bilan[:profondeur]) ? parse(Int, bilan[:profondeur]) : nothing,
                    sens = (isnothing(bilan[:sens]) || bilan[:sens] == "+") ? 1 : -1
                )
                push!(critères, critère)
            else
                @error("Le niveau de tri lexicographique « $(bilan[:niveau]) » n’existe pas, il ne sera donc pas utilisé.")
            end
        else
            @error("L’ensemble de critères lexicographiques « $expression_critère » est mal construit ou inconnu, il ne sera donc pas utilisé.")
        end
    end
    return critères
end

function créer_ordre_graphémique(graphèmes::AbstractVector, valeurs_tri::OrderedDict=OrderedDict{String, Vector{Int}}(), préindex::AbstractVector=Vector{Int}(), remplacements::AbstractDict=Dict(); casse::Union{Symbol, Nothing}=nothing)::NiveauOrdreLexicographique
    for (index, graphème) ∈ enumerate(graphèmes)
        index_complet = vcat(préindex, index)
        if graphème isa String
            if isnothing(casse)
                valeurs_tri[graphème |> lowercase] = valeurs_tri[graphème |> uppercase] = index_complet
            else
                fonctions = casse == :normale ? [uppercase, lowercase] : [lowercase, uppercase]
                graphème = [graphème |> fonction for fonction ∈ fonctions]
                créer_ordre_graphémique(graphème, valeurs_tri, index_complet, remplacements)
            end
        elseif graphème isa Pair
            for fonction ∈ [lowercase, uppercase]
                push!(remplacements, graphème.first |> fonction => graphème.second |> fonction)
            end
        elseif graphème isa Vector
            créer_ordre_graphémique(graphème, valeurs_tri, index_complet, remplacements; casse)
        end
    end
    modèle = créer_modèle(valeurs_tri |> keys |> collect)
    niveau = NiveauOrdreLexicographique("graphèmes", modèle, valeurs_tri, remplacements)
    return niveau
end

"Fonction créant un ordre spécial au sens où ce dernier est subordonné à l’ordre graphémique : il détecte tous les graphèmes généraux afin de prendre en compte la position de ses propres éléments."
function créer_ordre_spécial(nom::AbstractString, graphèmes::AbstractVector{<:AbstractString}, prémodèle::Union{Regex, Nothing}=nothing)::NiveauOrdreLexicographique
    modèle = créer_modèle(graphèmes, prémodèle)
    valeurs_tri = OrderedDict(graphème => [position] for (position, graphème) ∈ enumerate(graphèmes))
    return NiveauOrdreLexicographique(; nom, modèle, valeurs_tri)
end

function créer_modèle(graphèmes::AbstractVector, prémodèle::Union{Regex, Nothing}=nothing; casse_sensible::Bool=false)::Regex
    drapeau_casse = casse_sensible ? "" : "i"
    expression = @pipe graphèmes |> sort(_; by=length, rev=true) |> join(_, "|") |> protéger
    if !isnothing(prémodèle)
        expression = join([prémodèle.pattern, expression], "|")
    end
    return Regex(expression, drapeau_casse)
end

function trier_entités(expression::AbstractString, ordre::OrdreLexicographique; casse_sensible::Bool=false)::Vector
    expression = reduce(replace, ordre.remplacements, init=expression)
    résultat = []
    for critère ∈ ordre.critères
        sousrésultat = trier_entités(expression, critère.niveau.modèle, critère.niveau.valeurs_tri; sens=critère.sens, casse_sensible)
        if critère.niveau.nom == "graphèmes"
            if !isnothing(critère.profondeur)
                if critère.profondeur ≤ length(sousrésultat.valeur)
                    push!(résultat, sousrésultat.valeur[critère.profondeur])
                end
            else
                push!(résultat, sousrésultat.valeur)
            end
        else
            push!(résultat, sousrésultat.valeur)
        end
    end
    # println(expression, "\t", résultat)
    return résultat
end

function trier_entités(expression::AbstractString, modèle::Regex, valeurs_tri::OrderedDict{String, Vector{Int}}; sens::Int=1, casse_sensible::Bool=false)::NamedTuple{(:valeur, :longueur), Tuple{Vector, Int}}
    if !casse_sensible
        expression = lowercase(expression)
    end
    valeurs_brutes = [get(valeurs_tri, segment.match, 0) for segment ∈ eachmatch(modèle, expression)]
    if !isempty(valeurs_brutes)
        résultat = []
        taille = @pipe valeurs_brutes .|> length |> max(_...)
        valeurs = [vcat(valeur, fill(0, taille - length(valeur))) for valeur ∈ valeurs_brutes]
        for valeur ∈ zip(valeurs...)
            push!(résultat, valeur .* sens)
        end
    else
        @error("Aucun tri n’a pu être réalisé sur l’expression « $expression » : manquerait-il un graphème dans l’ordre lexicographique ?")
        résultat = [Inf]
    end
    longueur = length(valeurs_brutes)
    return (valeur=résultat, longueur=longueur)
end

function protéger(expression::AbstractString)
    caractères = ["?", "+", "*", ".", "(", ")", "^", "\$"]
    remplacements = @. caractères => "\\" * caractères
    expression_protégée = reduce(replace, remplacements, init=expression)
    return expression_protégée
end

end
