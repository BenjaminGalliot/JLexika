module Lexika

include("structures.jl")
include("trieur.jl")
include("générateur.jl")
include("créateur.jl")

using .Structures
using .Trieur
using .Générateur
using .Créateur
using FilePaths
using LoggingExtras

export générer_dictionnaire

function générer_dictionnaire(chemin_accès_configuration::Union{SystemPath, AbstractString}; créer_latex::Bool=true, créer_pdf::Bool=true, créer_html::Bool=false, nom_journal::AbstractString="journal.log", taille_échantillon::Union{Nothing, Int}=nothing, date::AbstractString="", forcer_compilation_latex::Bool=false, nettoyer_latex::Bool=true)
    chemin_configuration = chemin_accès_configuration |> Path |> abspath
    chemin_journal = joinpath(parent(chemin_configuration), nom_journal)
    journalisateur = TeeLogger(FileLogger(chemin_journal), ConsoleLogger())
    # global_logger(journalisateur)
    @info("Journal créé : $chemin_journal")
    with_logger(journalisateur) do
        configurer(chemin_configuration)
        créer_dictionnaire(; taille_échantillon, date)
        if créer_latex
            créer_fichiers_latex(; créer_pdf, forcer=forcer_compilation_latex, effacer_fichiers_temporaires=nettoyer_latex)
        end
    end
end

end
