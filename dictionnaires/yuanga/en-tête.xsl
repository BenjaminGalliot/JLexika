<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:variable name="couleurdéf">black</xsl:variable>
<xsl:variable name="couleurnua">Sepia</xsl:variable>
<xsl:variable name="couleurfra">black</xsl:variable>
<xsl:variable name="couleureng">black</xsl:variable>
<xsl:variable name="couleurlat">black</xsl:variable>
<xsl:variable name="couleurnumérodhomonyme">BrickRed</xsl:variable>
<xsl:variable name="couleurnomdegroupe">OliveGreen</xsl:variable>
<xsl:variable name="couleurnumérodesens">Fuchsia</xsl:variable>
<xsl:variable name="couleurpartiedudiscours">Blue</xsl:variable>
<xsl:variable name="couleurrégion">Green</xsl:variable>
<xsl:variable name="couleurphonologie">Purple</xsl:variable>
<xsl:variable name="couleurphonétique">Emerald</xsl:variable>
<xsl:variable name="couleurgraphique">Red</xsl:variable>
<xsl:variable name="couleurgloseinterlinéaire">Maroon</xsl:variable>
<xsl:variable name="couleurhyperrefurl">RoyalPurple</xsl:variable>
<xsl:variable name="couleurhyperreflink">Bittersweet</xsl:variable>
<xsl:variable name="couleurhyperrefcite">Cerulean</xsl:variable>

<xsl:template name="ajouter_première_de_couverture">
\includepdf[pages=1,fitpaper,trim=36.1cm 1.5cm 11.6cm 1.5cm]{couverture.pdf}
</xsl:template>

<xsl:template name="ajouter_quatrième_de_couverture">
\includepdf[pages=1,fitpaper,trim=11.6cm 1.5cm 36.1cm 1.5cm]{couverture.pdf}
</xsl:template>

<xsl:template name="ajouter_entête_générale">
% !TEX program = lualatex

\documentclass[type=book]{dictionnaire lexica}
\RequireVersions{
    *{application}{luaTeX} {0000/00/00 v1.18.0}
}
\hypersetup{urlcolor=<xsl:value-of select="$couleurhyperrefurl"/>, linkcolor=<xsl:value-of select="$couleurhyperreflink"/>, citecolor=<xsl:value-of select="$couleurhyperrefcite"/>}
\RequirePackage{relsize}
\graphicspath{{Lien vers images}}
\ConfigurerLangue[main]{fra}{}
\ConfigurerLangue{}{}[nua]
\ConfigurerLangue{eng}{}
\ConfigurerLangue{api}{}
\ConfigurerLangue{lat}{}
\RequirePackage[fit]{truncate}
\setmainfont{EBGaramond}
\NewDocumentCommand \pdéf { m } {\textesymbolique{#1}}
\NewDocumentCommand \pnua { m } {\textenua{\textcolor{<xsl:value-of select="$couleurnua"/>}{#1}}}
\NewDocumentCommand \papi { m } {\texteapi{#1}}
\NewDocumentCommand \pfra { m } {\textefra{\textcolor{<xsl:value-of select="$couleurfra"/>}{#1}}}
\NewDocumentCommand \peng { m } {\texteeng{\textcolor{<xsl:value-of select="$couleureng"/>}{#1}}}
\NewDocumentCommand \plat { m } {\textelat{\textcolor{<xsl:value-of select="$couleurlat"/>}{#1}}}
\RenewDocumentCommand \numérodhomonyme { m } {\textefra{\textcolor{<xsl:value-of select="$couleurnumérodhomonyme"/>}{#1}}}
\RenewDocumentCommand \nomdegroupe { m } {\unskip\enskip\textesymbolique{\textcolor{<xsl:value-of select="$couleurnomdegroupe"/>}{\cerclé{#1}}} \ignorespaces}
\RenewDocumentCommand \numérodesens { m } {\unskip\enskip\textesymbolique{\textcolor{<xsl:value-of select="$couleurnumérodesens"/>}{\cerclé{#1}}} \ignorespaces}
\RenewDocumentCommand \partiedudiscours { m } {\ignorespaces{\addfontfeatures{Letters=UppercaseSmallCaps}\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{\emph{#1}}\ignorespaces\ignorespacesafterend}}
\RenewDocumentCommand \définition { m } {\pdéf{►} #1}{}
\RenewDocumentCommand \glose { m } {\pdéf{▻} #1}
\RenewDocumentEnvironment {variantes} {} {\pfra{(var.}}{\unskip\pfra{)}}
\RenewDocumentEnvironment {étymologie} {} {\pfra{Étym. :}}{\ignorespacesafterend}
\RenewDocumentCommand \étymon { m } {\pnua{\textbf{#1}}}
\RenewDocumentCommand \nomvernaculaire { m } {\peng{#1}}
\RenewDocumentCommand \variante { m } {\pnua{\textcolor{<xsl:value-of select="$couleurnua"/>}{#1}}}
\RenewDocumentCommand \région { m } {\pfra{\addfontfeatures{Letters=UppercaseSmallCaps}\textcolor{<xsl:value-of select="$couleurrégion"/>}{[#1]}}}
\RenewDocumentCommand \relationsémantique { m m } {\pfra{#1 : }\pnua{#2}}
\RenewDocumentCommand \glosecourte { m } {\pfra{‘#1’}}
\RenewDocumentCommand \auteur { m } {\pfra{d’après \textsc{#1}}}
\RenewDocumentCommand \morphologie { m } {\pfra{Morph. : }#1}
\RenewDocumentCommand \emprunt { m } {\pfra{Empr. : }#1}
\RenewDocumentCommand \note { m } {\pfra{Note : }#1}
\NewDocumentCommand \notephonétique { m } {\pfra{(}#1\pfra{)}}
\NewDocumentCommand \noteglose { m m } {\pfra{Note : }#1 ‘#2’}
\NewDocumentCommand \stylelx { m } {{\textcolor{<xsl:value-of select="$couleurnua"/>}{\pnua{#1}}}}  % couleur puis police, sinon la police déborde sur le bloc suivant…
\NewDocumentCommand \styleph { m } {\formephonétique{#1}}
\NewDocumentCommand \styledialx { m } {\région{#1}}
\RenewDocumentCommand \renvoi { m m } {\pdéf{▻} \pnua{\cible{#1}{#2}}}

\hypersetup{pdfkeywords={dictionnaire, lexica, lexika, yuanga, nua}, pdfsubject={Dictionnaire yuanga – français}, pdftitle={Dictionnaire yuanga – français}}

\RenewDocumentCommand \pagedecollection { O{\languefra} } {
    \clearpage
    #1
    \thispagestyle{collection}

    \vspace*{\fill}

    {
        \centering

        \logocollectionlexica[0.2\textwidth]

        \bigskip

        \textegrisé{Directeur de collection et responsable informatique}

        \textbf{\plettresstylisées{\benjamin}}

        \bigskip

        \textegrisé{Responsable scientifique}

        \textbf{\plettresstylisées{\guillaume}}

        \bigskip

        \begin{center}
            \begin{varwidth}{\linewidth}
                Dictionnaires de la collection déjà réalisés :
                \begin{enumerate}
                    \item Japhug – chinois – français.
                    \item Zuanga-yuanga – français.
                    \item Na – chinois – français – anglais.
                \end{enumerate}
            \end{varwidth}
        \end{center}
    }

    \vfill\vfill
}

\RenewDocumentCommand \placerlogos {} {
    \AddToShipoutPictureBG*{
        \AtTextLowerLeft{
            \makebox[\textwidth][r]{
                \raisebox{0\height}{\includegraphics[keepaspectratio, height=1cm]{logo lacito.png}}
                \raisebox{-0.1\height}{\includegraphics[keepaspectratio, width=2.5cm]{logo province nord.png}}
                \raisebox{0\height}{\includegraphics[keepaspectratio, height=0.9cm]{logo efl.png}}
                \enskip
                \raisebox{0\height}{\includegraphics[width=1.5cm, keepaspectratio]{logo cnrs.pdf}}
            }
        }
    }
}

\RenewDocumentCommand \pagededédicace {} {
    \clearpage
    \thispagestyle{dédicace}
    \vspace*{\fill}

    À la communauté des locuteurs du zuanga-yuanga…

    \bigskip

    \noindent
    \hfill
    \includegraphics[keepaspectratio, height=6cm]{locuteurs 8.jpg}
    \hfill
    \includegraphics[keepaspectratio, height=6cm]{locuteurs 1.jpg}
    \hspace*{\fill}

    \bigskip

    \noindent
    \hfill
    \includegraphics[keepaspectratio, height=6cm]{locuteurs 6.jpg}
    \hfill
    \includegraphics[keepaspectratio, height=6cm]{locuteurs 2.jpg}
    \hspace*{\fill}

    \bigskip

    \noindent
    \hfill
    \includegraphics[keepaspectratio, height=5.5cm]{locuteurs 3.jpg}
    \hfill
    \includegraphics[keepaspectratio, height=5.5cm]{locuteurs 7.jpg}
    \hfill
    \includegraphics[keepaspectratio, height=5.5cm]{locuteurs 5.jpg}
    \hspace*{\fill}

    \bigskip

    \hfill
    … et en hommage à Françoise Ozanne-Rivierre et à Jean-Claude Rivierre.

    \vfill
}

<!-- \babelhyphenation{
  Rham-na-cées
} -->
</xsl:template>

<xsl:template name="ajouter_entête_alphabétique">
\RenewDocumentCommand \pnua { m } {\textenua{\textcolor{<xsl:value-of select="$couleurnua"/>}{\smaller[0.5]#1}}}
\RenewDocumentCommand \papi { m } {\texteapi{\smaller[0.5]#1}}
\RenewDocumentCommand \vedetteentête { m } {\pnua{#1}}
\RenewDocumentCommand \vedetteentêtecourte { m } {\truncate{5cm}{\pnua{#1}}}
\RenewDocumentCommand \vedette { m } {\pnua{\textbf{\larger[0.5]#1}}}
\RenewDocumentCommand \sousvedette { m } {\pnua{\textbf{#1}}}
\RenewDocumentCommand \lienbrisé { m } {#1}
\setlength{\seuiltitrebasdepage}{6\baselineskip}

\ExplSyntaxOn
\seq_set_split:Nnn \g_listecaractèrespageimpaire_seq {,} {b, bw, c~–~ç, ch, d, dr, f, fh, g, ng, h, j, k, kh, l, lh, m, mw, mh, mhw, ń~–~n, ng, ny, ńh~–~nh, nhy, p, pw, ph, phw, r, rh, s, t, tr, th, thr, v, vh, vw, w, wh, x, y, yh, z}
\seq_set_split:Nnn \g_listecaractèrespagepaire_seq {,} {a~–~ã~–~â, e~–~è~–~ẽ~–~ê, i~–~î, o~–~ò~–~õ~–~ô~–~ö, u~–~û~–~ü}

\fancypagestyle{dictionnaireaveccaractères}[dictionnaire]{
    \fancyfoot[OC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespagepaire_seq}[,~]}}
    \fancyfoot[EC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespageimpaire_seq}[,~]}}
}

\RenewDocumentEnvironment {dictionnaire} { O{\nomdictionnaire} } {
    \cleardoublepage
    \RenewDocumentCommand \chaptermark { m } {}
    \RenewDocumentCommand \sectionmark { m } {}
    \fancypagestyle{plain}[base]{
        \fancyfoot[OC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespagepaire_seq}[,~]}}
        \fancyfoot[EC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespageimpaire_seq}[,~]}}
    }
    \markboth{}{}
    \str_if_eq:eeF {\tl_to_str:n {#1}} {\tl_to_str:n {\nomdictionnaire}} {
        \RenewDocumentCommand \nomdictionnaire {} {#1}
    }
    \pagestyle{dictionnaireaveccaractères}
    \chapter{#1}
} {
    \newpage
}
\ExplSyntaxOff
</xsl:template>

<xsl:template name="ajouter_entête_thématique">
\RenewDocumentCommand \pnua { m } {\textenua{\textcolor{<xsl:value-of select="$couleurnua"/>}{\smaller[0.5]#1}}}
\RenewDocumentCommand \papi { m } {\texteapi{\smaller[0.5]#1}}
\RenewDocumentCommand \vedetteentête { m } {\pnua{#1}}
\RenewDocumentCommand \vedetteentêtecourte { m } {\truncate{5cm}{\pnua{#1}}}
\RenewDocumentCommand \vedette { m } {\pnua{\textbf{\larger[0.5]#1}}}
\RenewDocumentCommand \sousvedette { m } {\pnua{\textbf{#1}}}
\setlength{\seuiltitrebasdepage}{0em}

\ExplSyntaxOn
\RenewDocumentEnvironment {dictionnaire} { O{\nomdictionnaire} } {
    \cleardoublepage
    \RenewDocumentCommand \chaptermark { m } {}
    \RenewDocumentCommand \sectionmark { m } {}
    \markboth{}{}
    \str_if_eq:eeF {\tl_to_str:n {#1}} {\tl_to_str:n {\nomdictionnaire}} {
        \RenewDocumentCommand \nomdictionnaire {} {#1}
    }
    \pagestyle{dictionnaire}
    \chapter{#1}
} {
    \newpage
}
\ExplSyntaxOff

\titleformat{\section}{\normalfont\normalsize\bfseries}{\thesection}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\subsection}{\normalfont\normalsize\bfseries}{\thesubsection}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\subsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\subsubsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsubsection}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\subsubsubsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsubsubsection}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\paragraph}[runin]{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}
\titleformat{\subparagraph}[runin]{\normalfont\normalsize\bfseries}{\thesubparagraph}{1em}{#1\hspace*{1em}\xrfill[.5ex]{.1pt}[grisé]}

\titlespacing{\chapter}{0pt}{50pt}{40pt}
\titlespacing{\section}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing{\subsection}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing{\subsubsection}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing{\subsubsubsection}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing{\subsubsubsubsection}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
</xsl:template>

<xsl:template name="ajouter_entête_inverse">
\RenewDocumentCommand \pnua { m } {\textenua{\textcolor{<xsl:value-of select="$couleurnua"/>}{\smaller[0.5]#1}}}
\RenewDocumentCommand \papi { m } {\texteapi{\smaller[0.5]#1}}
\RenewDocumentCommand \vedetteentête { m } {\pfra{#1}}
\RenewDocumentCommand \vedetteentêtecourte { m } {\truncate{3cm}{\pfra{#1}}}
\RenewDocumentCommand \vedette { m } {\pfra{#1}}
\RenewDocumentCommand \sousvedette { m } {\pfra{\textbf{#1}}}

\ExplSyntaxOn
\seq_set_split:Nnn \g_listecaractèrespageimpaire_seq {,} {b, c~–~ç, d, f, g, h, j, k, l, m, n, p, q, r, s, t, v, w, x, z}
\seq_set_split:Nnn \g_listecaractèrespagepaire_seq {,} {a~–~à~–~â~–~æ, e~–~é~–~è~–~ê~–~ë, i~–~î~–~ï, o~–~ô~–~œ, u~–~ù~–~û~–~ü, y~–~ÿ}

\fancypagestyle{dictionnaireaveccaractères}[dictionnaire]{
    \fancyfoot[OC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespagepaire_seq}[,~]}}
    \fancyfoot[EC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespageimpaire_seq}[,~]}}
}

\RenewDocumentEnvironment {dictionnaire} { O{\nomdictionnaire} } {
    \cleardoublepage
    \RenewDocumentCommand \chaptermark { m } {}
    \RenewDocumentCommand \sectionmark { m } {}
    \fancypagestyle{plain}[base]{
        \fancyfoot[OC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespagepaire_seq}[,~]}}
        \fancyfoot[EC]{\tiny\pnua{\afficherlettrines{\g_listecaractèrespageimpaire_seq}[,~]}}
    }
    \markboth{}{}
    \str_if_eq:eeF {\tl_to_str:n {#1}} {\tl_to_str:n {\nomdictionnaire}} {
        \RenewDocumentCommand \nomdictionnaire {} {#1}
    }
    \pagestyle{dictionnaireaveccaractères}
    \chapter{#1}
} {
    \newpage
}
\ExplSyntaxOff

\titleformat{\section}{\normalfont\Large\bfseries}{\thesection}{1em}{#1}
\titleformat{\subsection}{\normalfont\large\bfseries}{\thesubsection}{1em}{#1}
\titleformat{\subsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{#1}
\titleformat{\subsubsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsubsection}{1em}{#1}
\titleformat{\subsubsubsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsubsubsection}{1em}{#1}
\titleformat{\paragraph}[runin]{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{#1}
\titleformat{\subparagraph}[runin]{\normalfont\normalsize\bfseries}{\thesubparagraph}{1em}{#1}

\titlespacing{\chapter}{0pt}{50pt}{40pt}
\titlespacing{\section}{0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}
\titlespacing{\subsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
\titlespacing{\subsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
\titlespacing{\subsubsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
\titlespacing{\subsubsubsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
\titlespacing{\paragraph}{0pt}{3.25ex plus 1ex minus .2ex}{1em}
\titlespacing{\subparagraph}{\parindent}{3.25ex plus 1ex minus .2ex}{1em}
</xsl:template>

<xsl:template name="ajouter_entête_introduction">
\addbibresource{bibliographie.bib}

\RenewDocumentCommand \pnua { m } {\textenua{\textcolor{<xsl:value-of select="$couleurnua"/>}{\smaller[0.5]#1}}}
\RenewDocumentCommand \papi { m } {\texteapi{\smaller[0.5]#1}}
\NewDocumentCommand \pnuabis { m } {\emph{\pnua{#1}}}
\NewDocumentCommand \lnua { } {\color{<xsl:value-of select="$couleurnua"/>}\languenua\smaller[0.5]}
\NewDocumentCommand \gloseinterlinéaire { m } {\textsc{\textcolor{<xsl:value-of select="$couleurgloseinterlinéaire"/>}{#1}}}

\RenewDocumentCommand \multicitedelim {} {\addcomma\space}
\RenewDocumentCommand \compcitedelim {} {\addcomma\space}
\RenewDocumentCommand \postnotedelim {} {\,\addcolon\space}
\RenewDocumentCommand \finalnamedelim {} {\addspace\textsc{\&amp;}\addspace} %delimiter of the final name
\RenewDocumentCommand \multinamedelim {} {\addspace\textsc{\&amp;}\addspace} %[GVW] 1/7/2021: delimiter of the non-final name(s) in lists with more than two names is no different
\RenewDocumentCommand \finallistdelim {} {\addspace\textsc{\&amp;}\addspace}
\DefineBibliographyStrings{french}{%
  andothers = {et coll.}
}

% Basculement des références anglaises en style français (noms en petites capitales) dans la bibliographie.
\DefineBibliographyExtras{french}{
  \RenewDocumentCommand {\mkbibnamefamily} { m } {\textnohyphenation{#1}}
}

% Éviter le formatage brutal retirant presque toutes les capitales.
\DeclareFieldFormat[article, book, thesis, incollection, unpublished, inproceedings]{titlecase}{\NoCaseChange{#1}}

\ExplSyntaxOn
\newbibmacro*{cite:labelyear+extrayear}{
  \iffieldundef{labelyear}
    {}
    {\printtext[bibhyperref]{
      \iffieldequalstr{labelyear}{Ongoing}
        {ongoing}
        {\printfield{labelyear}}
      \printfield{extrayear}}}}
\ExplSyntaxOff

\AtEveryBibitem{%
  \clearfield{doi}
  \clearfield{eprint}
}

\SetTblrStyle{foot}{font=\footnotesize}

\ExplSyntaxOn
\DefTblrTemplate{note-tag}{default}{\InsertTblrNoteTag\dotFFN}  % \dotFFN provient de babel-french.
\DefTblrTemplate{note-sep}{default}{\kernFFN}  % \kernFFN provient de babel-french.
\DefTblrTemplate{note}{default}{
  \MapTblrNotes{
    \noindent
    \UseTblrTemplate{note-tag}{default}
    \UseTblrTemplate{note-sep}{default}
    \UseTblrTemplate{note-text}{default}
    \par
  }
}
\ExplSyntaxOff
\DefTblrTemplate{contfoot-text}{default}{Suite page suivante…}
\DefTblrTemplate{conthead-text}{default}{(suite)}
\newlength{\espacementverticaltableau}
\setlength{\espacementverticaltableau}{\bigskipamount}
\SetTblrOuter[longtblr]{presep=\espacementverticaltableau, postsep=\espacementverticaltableau}
\SetTblrOuter[tblr]{presep=\espacementverticaltableau, postsep=\espacementverticaltableau}

\RequirePackage{colortbl}
\definecolor{gris}{gray}{0.9}
\def\frenchtablename{\scshape Tableau}
\counterwithout{table}{chapter}
\counterwithout{figure}{chapter}
\RequirePackage{caption}
% \captionsetup[table]{skip=5pt}
\renewcommand{\thechapter}{\Roman{chapter}}
\renewcommand{\thesection}{\arabic{section}}

\NewDocumentCommand \phonologie { m } {\textcolor{<xsl:value-of select="$couleurphonologie"/>}{\papi{/#1/}}}
\NewDocumentCommand \phonologiebis { m } {\textcolor{<xsl:value-of select="$couleurphonologie"/>}{\papi{#1}}}
\NewDocumentCommand \phonétique { m } {\textcolor{<xsl:value-of select="$couleurphonétique"/>}{\papi{[#1]}}}
\NewDocumentCommand \phonétiquebis { m } {\textcolor{<xsl:value-of select="$couleurphonétique"/>}{\papi{#1}}}
\NewDocumentCommand \graphique { m } {\textcolor{<xsl:value-of select="$couleurgraphique"/>}{\papi{&lt;#1&gt;}}}
\NewDocumentCommand \graphiquebis { m } {\textcolor{<xsl:value-of select="$couleurgraphique"/>}{\papi{#1}}}
\NewDocumentCommand \régionbis { m } {\pfra{\addfontfeatures{Letters=UppercaseSmallCaps}\textcolor{<xsl:value-of select="$couleurrégion"/>}{#1}}}

% https://github.com/lvjr/tabularray/issues/434#issuecomment-1686957209
\RequirePackage{regexpatch}
% tested with tabularray 2023A (2023-03-01) and 2024A (2024-02-16) only
\IfPackageAtLeastTF{tabularray}{2024-02-17}{
  \PackageWarning { patch-tabularray }
    { Experimental~patching~to~support~"valign=M"~may~fail~in~newer~versions. }
}{}
\ExplSyntaxOn
\makeatletter
\tl_const:Nn \c__tblr_valign_M_tl {M}
\tl_new:N    \g__tblr_cell_valign_sub_tl

\cs_undefine:N \__tblr_get_cell_alignments:nn
\cs_new_protected:Npn \__tblr_get_cell_alignments:nn #1 #2
  {
    \group_begin:
    \tl_gset:Nx \g__tblr_cell_halign_tl
      { \__tblr_data_item:neen { cell } {#1} {#2} { halign } }
    \tl_set:Nx \l__tblr_v_tl
      { \__tblr_data_item:neen { cell } {#1} {#2} { valign } }
    \tl_case:NnF \l__tblr_v_tl
      {
        \c__tblr_valign_t_tl
          {
            \tl_gset:Nn \g__tblr_cell_valign_tl {m}
            \tl_gset:Nn \g__tblr_cell_middle_tl {t}
            \tl_gclear:N \g__tblr_cell_valign_sub_tl
          }
        \c__tblr_valign_m_tl
          {
            \tl_gset:Nn \g__tblr_cell_valign_tl {m}
            \tl_gset:Nn \g__tblr_cell_middle_tl {m}
            \tl_gclear:N \g__tblr_cell_valign_sub_tl
          }
        \c__tblr_valign_M_tl
          {
            \tl_gset:Nn \g__tblr_cell_valign_tl {m}
            \tl_gset:Nn \g__tblr_cell_valign_sub_tl {M}
            \tl_gset:Nn \g__tblr_cell_middle_tl {t}
          }
        \c__tblr_valign_b_tl
          {
            \tl_gset:Nn \g__tblr_cell_valign_tl {m}
            \tl_gset:Nn \g__tblr_cell_middle_tl {b}
            \tl_gclear:N \g__tblr_cell_valign_sub_tl
          }
      }
      {
        \tl_gset_eq:NN \g__tblr_cell_valign_tl \l__tblr_v_tl
        \tl_gclear:N \g__tblr_cell_middle_tl
      }
    \group_end:
  }

% \tracingxpatches

% make \__tblr_build_cell_content:NN patchable
\char_set_catcode_space:n {32}
\xpatchcmd \__tblr_build_cell_content:NN
  {\hrule height 0pt}
  {\hrule height0pt}
  {} {\PackageError{patch-tabularray}{Patch~failed.}{}}
\char_set_catcode_ignore:n {32}

\IfPackageAtLeastTF{tabularray}{2024-02-16}{
  % 2024A or newer
  % use a longer patten in the hope of failing fast
  \xpatchcmd \__tblr_build_cell_content:NN
    {
      \int_compare:nNnT { \lTblrCellRowSpanTl } &lt; {2}
        {
          \box_set_ht:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-upper } }
          \box_set_dp:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-lower } }
        }
    }
    {
      \bool_lazy_and:nnT
        { \int_compare_p:nNn { \lTblrCellRowSpanTl } &lt; {2} }
        { ! \tl_if_eq_p:NN \c__tblr_valign_M_tl \g__tblr_cell_valign_sub_tl }
        {
          \box_set_ht:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-upper } }
          \box_set_dp:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-lower } }
        }
    }
    { } { \PackageError { patch-tabularray } { Patch~failed } {} }
}{
  % prior to 2024A
  \xpatchcmd \__tblr_build_cell_content:NN
    {
      \int_compare:nNnT { \l__tblr_cell_rowspan_tl } &lt; {2}
        {
          \box_set_ht:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-upper } }
          \box_set_dp:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-lower } }
        }
    }
    {
      \bool_lazy_and:nnT
        { \int_compare_p:nNn { \l__tblr_cell_rowspan_tl } &lt; {2} }
        { ! \tl_if_eq_p:NN \c__tblr_valign_M_tl \g__tblr_cell_valign_sub_tl }
        {
          \box_set_ht:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-upper } }
          \box_set_dp:Nn \l__tblr_a_box
            { \__tblr_data_item:nen { row } {#1} { @row-lower } }
        }
    }
    { } { \PackageError { patch-tabularray } { Patch~failed } {} }
}

\keys_define:nn { tblr-column }
  {
    M .meta:n = { valign = M },
  }

\makeatother
\ExplSyntaxOff

\setlength{\textfloatsep}{5pt plus 2.0pt minus 2.0pt}
\setlength{\floatsep}{5pt plus 2.0pt minus 2.0pt}
% \setlength{\intextsep}{5pt plus 2.0pt minus 2.0pt}

\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}

\RequirePackage{langsci-gb4e}
% \renewcommand{\exfont}{\itshape}
\newfontfamily{\policeglose}{Gentium Plus}[StylisticSet={05}] % gb4e n’accepte pas un changement de langue (\languenua, \selectlanguage, \lnua, etc.).
\newfontfamily{\policerégion}{EBGaramond} % gb4e n’accepte pas un changement de langue (\languenua, \selectlanguage, \lnua, etc.), même dans une commande qui force un changement…
\NewDocumentCommand \régionter { m }{{\normalfont\policerégion\addfontfeatures{Letters=UppercaseSmallCaps}\textcolor{<xsl:value-of select="$couleurrégion"/>}{[#1]}}}

\renewcommand{\eachwordone}{\itshape\color{<xsl:value-of select="$couleurnua"/>}\policeglose\smaller[0.5]}

% Ajout d’une « glose » à une seule ligne (sans glose interlinéaire), afin de bénéficier de l’uniformisation avec les autres gloses…
\makeatletter
\gdef\onesent#1\\{% #1 = first line
    \getwords(\lineone,\eachwordone)#1 \\%
    % \getwords(\linetwo,\eachwordtwo)#2 \\%
    \loop\lastword{\eachwordone}{\lineone}{\wordone}%
        %  \lastword{\eachwordtwo}{\linetwo}{\wordtwo}%
         \global\setbox\gline=\hbox{\unhbox\gline
                                    \hskip\glossglue
                                    \vtop{\box\wordone   % vtop was vbox
                                          % \nointerlineskip
                                          % \box\wordtwo
                                         }%
                                   }%
         \testdone
         \ifnotdone
    \repeat
    \egroup % matches \bgroup in \gloss
\gl@stop}

\def\glx%                  % Introduces 1-line text.
   {\raggedright%
     \bgroup %\begin{flushleft}
     \ifx\@gsingle1%
	 \def\baselinestretch{1}\@selfnt\fi
    \bgroup
    \onesent
}
\makeatother

\RequirePackage{tikz}

\babelhyphenation[french]{
  labio-vé-lai-res
  bi-labiales
  labio-den-tales
  inter-den-tales
  den-tales
  post-alvéo-laires
  pala-tales
  vé-lai-res
  laryn-gales
}

\RequirePackage{amsmath}
\RequirePackage{calc}
\RequirePackage{graphicx}
\RequirePackage{luacolor}
\RequirePackage[soul]{lua-ul}
</xsl:template>

<xsl:template name="ajouter_premières_pages">
\input{variables}

\pagedetitre{
    \titre

    \medskip

    {\smaller[1]\complémenttitre}

    \medskip

    {\smaller[2]\normalfont\soustitre}
}

\pagedecollection

\pagedetitrecomplète{
    \titre

    \medskip

    {\smaller[1]\complémenttitre}

    \medskip

    {\smaller[2]\normalfont\soustitre}
}{
    \textbf{\plettresstylisées{\isabelle}}

    \medskip

    {\normalsize\normalfont avec la collaboration de}

    \medskip

    \textbf{\plettresstylisées{Alice \textsc{Poy-Yethy}}}\\
    \textbf{\plettresstylisées{Joachim \textsc{Pebu-Polae}}}

    {\normalsize\normalfont et le}

    \textbf{Comité linguistique zuanga-yuanga}

    \bigskip

    \textegrisé{Conception et ingénierie lexicographiques et artistiques}

    \textbf{\plettresstylisées{\benjamin}}
}[]{}

\pagedesinformations{
    titre = {\titre{} \complémenttitre},
    linguiste = {\isabelle},
    rôleslinguiste = {collecte des données, analyse linguistique (phonologie, grammaire, lexicologie et lexicographie) et rédaction},
    concepteur = {\benjamin},
    autresauteurs = {},
    rôlesconcepteur = {conception et ingénierie lexicographiques et artistiques, développement informatique, conception de la couverture et rédaction de la postface},
    directeurcollection = {\benjamin},
    responsablescientifique = {\guillaume},
    codesource = {\url{https://gitlab.com/BenjaminGalliot/Lexika/-/tree/maîtresse/dictionnaires/yuanga}},
    identifiantHAL = {halshs-03902090v2},
    numéro = {2},
    ISBNnumérique = {aaa},
    ISBNpapier = {bbb},
    ISSNnumérique = {ccc},
    ISSNpapier = {ddd},
    polices = {
        \begin{itemize}
            \item Français, anglais et latin : EB Garamond 12\\\url{https://github.com/octaviopardo/EBGaramond12}
            \item Yuanga et API : Gentium Plus\\\url{https://software.sil.org/gentium}
            \item Symboles divers : Noto Sans Symbols 2\\\url{https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2}
        \end{itemize}
    },
    divers = {Photo de couverture, représentant l’intérieur d’une maison kanak au nord de la Nouvelle-Calédonie, réalisée par \isabelle{} en 2010.\\[\medskipamount]Nous remercions Raphaëlle Chossenot (chargée d’édition) et Claire Sauttreau (apprentie en édition), qui ont fait partie du projet de la collection Lexica durant l’année 2023.\\[\medskipamount]Imprimé en 2025 par l’imprimerie Paillart.
    }
}[\medskipamount]

\pagededédicace
</xsl:template>

<xsl:template name="ajouter_introduction">
\input{introduction.tex}
</xsl:template>

<xsl:template name="ajouter_postface">
\input{postface.tex}
</xsl:template>

</xsl:stylesheet>
