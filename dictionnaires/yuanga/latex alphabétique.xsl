<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:import href="en-tête.xsl"/>

<xsl:template match="RessourceLexicale">
<xsl:apply-templates select="." mode="alphabétique"/>
</xsl:template>

<xsl:template match="RessourceLexicale" mode="alphabétique">
<xsl:call-template name="ajouter_entête_générale"/>
<xsl:call-template name="ajouter_entête_alphabétique"/>

\begin{document}

\pagenumbering{roman}

<xsl:call-template name="ajouter_premières_pages"/>

\bibliographie

\mainmatter

\begin{dictionnaire}[\textefra{Dictionnaire alphabétique}]
<xsl:apply-templates select="InformationsLexicographiques" mode="alphabétique"/>
\end{dictionnaire}

\tabledesmatières

\end{document}

</xsl:template>

<xsl:template match="InformationsLexicographiques" mode="alphabétique">
    <xsl:apply-templates select="OrdreLexicographique" mode="alphabétique"/>
</xsl:template>

<xsl:template match="OrdreLexicographique" mode="alphabétique">
    <xsl:apply-templates select="Élément" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Élément" mode="alphabétique">
    <xsl:variable name="lettrine" select="if (Élément) then string-join(Élément, ' – ') else ." />
    <xsl:variable name="expression_rationnelle_graphèmes">
        <xsl:call-template name="créer_expression_rationnelle_graphèmes">
            <xsl:with-param name="graphèmes" select="."/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bloc_entrées">
        <xsl:apply-templates select="/RessourceLexicale/Dictionnaire/EntréesLexicales" mode="alphabétique">
            <xsl:with-param name="expression_rationnelle_graphèmes" select="$expression_rationnelle_graphèmes"/>
        </xsl:apply-templates>
    </xsl:variable>
    <xsl:if test="$bloc_entrées != ''">
        <!-- <xsl:value-of select="if (string-length(serialize($bloc_entrées)) &lt; 2000) then '\bigskip' else '\newpage'"/> -->
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\begin{bloclettrine}{</xsl:text>
        <xsl:value-of select="$lettrine"/>
        <xsl:text>}</xsl:text>
        <xsl:text>&#10;</xsl:text>
        <xsl:value-of select="$bloc_entrées"/>
        <xsl:text>\end{bloclettrine}</xsl:text>
        <xsl:text>&#10;</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="EntréesLexicales" mode="alphabétique">
    <xsl:param name="expression_rationnelle_graphèmes"/>
    <xsl:apply-templates select="EntréeLexicale[matches(replace(Lemme/Forme, '[_-]', ''), $expression_rationnelle_graphèmes, 'i;j')]" mode="alphabétique"/>
</xsl:template>

<xsl:template match="EntréeLexicale" mode="alphabétique">
    <xsl:text>\begin{entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme" mode="alphabétique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="NuméroDHomonyme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Lemme/FormePhonétique" mode="alphabétique"/>
    <xsl:apply-templates select="Lemme/Région" mode="alphabétique"/>
    <xsl:apply-templates select="Notes[Note[Type='phonétique']]" mode="alphabétique"/>
    <xsl:apply-templates select="Lemme/Variantes" mode="alphabétique"/>
    <xsl:apply-templates select="Groupes" mode="alphabétique"/>
    <xsl:apply-templates select="ListeDeSens" mode="alphabétique"/>
    <xsl:apply-templates select="Morphologie" mode="alphabétique"/>
    <xsl:apply-templates select="Étymologie" mode="alphabétique"/>
    <xsl:apply-templates select="Notes[Note[Type!='phonétique']]" mode="alphabétique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{entrée}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Groupes" mode="alphabétique">
    <xsl:for-each select="Groupe">
        <xsl:apply-templates select="." mode="alphabétique"/>
    </xsl:for-each>
</xsl:template>

<xsl:template match="Groupe" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{groupe}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="NomDeGroupe"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="PartieDuDiscours" mode="alphabétique"/>
    <xsl:apply-templates select="ListeDeSens" mode="alphabétique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{groupe}</xsl:text>
</xsl:template>

<xsl:template match="Sous-entréesLexicales" mode="alphabétique">
    <xsl:apply-templates select="Sous-entréeLexicale" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Sous-entréeLexicale" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{sous-entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme" mode="alphabétique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Lemme/Région" mode="alphabétique"/>
    <xsl:apply-templates select="Lemme/Variantes" mode="alphabétique"/>
    <xsl:apply-templates select="ListeDeSens" mode="alphabétique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{sous-entrée}</xsl:text>
</xsl:template>

<xsl:template match="FormePhonétique" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\formephonétique{</xsl:text>
        <xsl:call-template name="forme_phonétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Région" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\région{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Variantes" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{variantes}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Variante">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{variantes}</xsl:text>
</xsl:template>

<xsl:template match="Variante" mode="alphabétique">
    <xsl:text>\variante{</xsl:text>
    <xsl:apply-templates select="Forme" mode="alphabétique"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="FormePhonétique" mode="alphabétique"/>
    <xsl:apply-templates select="Région" mode="alphabétique"/>
</xsl:template>

<xsl:template match="PartieDuDiscours" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\partiedudiscours{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="ListeDeSens" mode="alphabétique">
    <xsl:apply-templates select="Sens" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Sens" mode="alphabétique">
    <xsl:apply-templates select="NuméroDeSens" mode="alphabétique"/>
    <xsl:apply-templates select="PartieDuDiscours" mode="alphabétique"/>
    <xsl:apply-templates select="Gloses" mode="alphabétique"/>
    <xsl:apply-templates select="Définitions" mode="alphabétique"/>
    <xsl:apply-templates select="Notes" mode="alphabétique"/>
    <xsl:apply-templates select="NomsScientifiques" mode="alphabétique"/>
    <xsl:apply-templates select="NomsVernaculaires" mode="alphabétique"/>
    <xsl:apply-templates select="Exemples" mode="alphabétique"/>
    <xsl:apply-templates select="Sous-entréesLexicales" mode="alphabétique"/>
    <xsl:apply-templates select="RelationsSémantiques" mode="alphabétique"/>
</xsl:template>

<xsl:template match="NuméroDeSens" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\numérodesens{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="DomainesSémantiques" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\domainesémantique{</xsl:text>
    <xsl:for-each select="DomaineSémantique">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:if test="position() != last()">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="DomaineSémantique" mode="alphabétique">
    <xsl:variable name="domainesémantique" select="."/>
    <xsl:value-of select="/RessourceLexicale/InformationsLexicographiques/OrdreThématique/Hiérarchie//Élément[Identifiant=$domainesémantique]/Valeur"/>
</xsl:template>

<xsl:template match="Définitions" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{définitions}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Définition[@langue = 'fra']">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{définitions}</xsl:text>
</xsl:template>

<xsl:template match="Définition" mode="alphabétique">
    <xsl:text>\définition{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Gloses" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{gloses}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Glose[@langue = 'fra']">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{gloses}</xsl:text>
</xsl:template>

<xsl:template match="Glose" mode="alphabétique">
    <xsl:text>\glose{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomsScientifiques" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{nomsscientifiques}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="NomScientifique">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:if test="position() != last()">
            <xsl:text> ; </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{nomsscientifiques}</xsl:text>
</xsl:template>

<xsl:template match="NomScientifique" mode="alphabétique">
    <xsl:text>\nomscientifique{</xsl:text>
    <xsl:value-of select="Binôme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Famille"/>
    <xsl:text>}{}{</xsl:text>
    <xsl:value-of select="Binôme"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomsVernaculaires" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{nomsvernaculaires}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="NomVernaculaire">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:if test="position() != last()">
            <xsl:text> ; </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{nomsvernaculaires}</xsl:text>
</xsl:template>

<xsl:template match="NomVernaculaire" mode="alphabétique">
    <xsl:text>\nomvernaculaire{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Exemples" mode="alphabétique">
    <xsl:apply-templates select="Exemple" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Exemple" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exemple}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="Original" mode="alphabétique"/>
    <xsl:for-each select="Traduction">
        <xsl:apply-templates select="." mode="alphabétique"/>
        <xsl:if test="not(position() = last())">
            <xsl:text>&#10;</xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exemple}</xsl:text>
</xsl:template>

<xsl:template match="Original" mode="alphabétique">
    <xsl:text>\original{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="../Région" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Traduction" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\traduction{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="RelationsSémantiques" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{relationssémantiques}</xsl:text>
    <xsl:for-each-group select="RelationSémantique" group-by="Type">
        <xsl:sort select="index-of(('renvoi', 'synonyme', 'antonyme'), current-grouping-key())"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\relationsémantique{</xsl:text>
        <xsl:text>\typerelationsémantique{</xsl:text>
        <xsl:call-template name="traduire">
            <xsl:with-param name="expression" select="current-grouping-key()"/>
        </xsl:call-template>
        <xsl:text>}}{</xsl:text>
        <xsl:for-each select="current-group()">
            <xsl:text>\ciblerelationsémantique{</xsl:text>
            <xsl:value-of select="replace(Cible, '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="Cible/@identifiant"/>
            <xsl:text>}</xsl:text>
            <xsl:apply-templates select="Région" mode="alphabétique"/>
            <xsl:apply-templates select="Glose[@langue = 'fra']" mode="alphabétique"/>
            <xsl:if test="not(position() = last())">
                <xsl:text>\pfra{, }</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:for-each-group>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{relationssémantiques}</xsl:text>
</xsl:template>

<xsl:template match="Étymologie" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{étymologie}</xsl:text>
    <xsl:apply-templates select="Étymon" mode="alphabétique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{étymologie}</xsl:text>
</xsl:template>

<xsl:template match="Étymologie[Étymon[Emprunt]]" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\emprunt{</xsl:text>
    <xsl:value-of select="Étymon/Emprunt"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Étymon" mode="alphabétique">
    <xsl:if test="preceding-sibling::Étymon">
        <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:apply-templates select="Langue" mode="alphabétique"/>
    <xsl:apply-templates select="Forme" mode="alphabétique"/>
    <xsl:apply-templates select="Glose" mode="alphabétique"/>
    <xsl:apply-templates select="Emprunt" mode="alphabétique"/>
    <xsl:apply-templates select="Auteur" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Étymon/Forme" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\étymon{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Étymon/Langue" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\langue{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Étymon/Auteur" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\auteur{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Étymon/Glose|RelationSémantique/Glose" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\glosecourte{</xsl:text>
    <xsl:apply-templates mode="alphabétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Morphologie" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\morphologie{</xsl:text>
    <xsl:apply-templates mode="alphabétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Notes" mode="alphabétique">
    <xsl:apply-templates select="Note" mode="alphabétique"/>
</xsl:template>

<xsl:template match="Note[not(Glose) and Type!='phonétique']" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\note{</xsl:text>
    <xsl:apply-templates select="Texte" mode="alphabétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Note[Type='phonétique']" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\notephonétique{</xsl:text>
    <xsl:apply-templates select="Texte" mode="alphabétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Note[Glose]" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\noteglose{</xsl:text>
    <xsl:apply-templates select="Texte" mode="alphabétique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Glose"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Forme" mode="alphabétique">
    <xsl:value-of select="replace(., '_', '')"/>
</xsl:template>

<xsl:template match="Texte" mode="alphabétique">
    <xsl:call-template name="adapter_langue"/>
</xsl:template>

<xsl:template match="lien" mode="alphabétique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\lien{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(., '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style" mode="alphabétique">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style[@type='ph']" mode="alphabétique">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
        <xsl:call-template name="forme_phonétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="text()" mode="alphabétique">
    <xsl:value-of select="replace(., '&quot;(.+?)&quot;', '« $1 »')"/>
</xsl:template>

<xsl:template name="adapter_langue">
    <xsl:text>\p</xsl:text>
    <xsl:value-of select="@langue"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates mode="alphabétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="forme_phonétique">
    <xsl:value-of select="replace(., ':', 'ː')"/>
</xsl:template>

<xsl:template name="lettrine_variante">
    <xsl:param name="lettrine"/>
    <xsl:if test="$lettrine = 'dr' or $lettrine = 'tr' or $lettrine = 'thr'">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>(variante de GOs)</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="traduire">
    <xsl:param name="expression"/>
    <xsl:choose>
        <xsl:when test="$expression='renvoi'">
            <xsl:text>Cf.</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='synonyme'">
            <xsl:text>Syn.</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='antonyme'">
            <xsl:text>Ant.</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$expression"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="créer_expression_rationnelle_graphèmes">  <!-- forme : ^(x|y|z) -->
    <xsl:param name="graphèmes"/>
    <xsl:text>^</xsl:text>
    <xsl:call-template name="exclure_graphèmes_chevauchants">
        <xsl:with-param name="graphèmes" select="$graphèmes"/>
    </xsl:call-template>
    <xsl:text>(</xsl:text>
    <xsl:choose>
        <xsl:when test="Élément">
            <xsl:for-each select="$graphèmes/Élément">
                <xsl:value-of select="."/>
                <xsl:if test="not(position() = last())">
                    <xsl:text>|</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$graphèmes"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template name="exclure_graphèmes_chevauchants">  <!-- forme : ^(?!xz|yz)(x|y) (attention : nécessite le drapeau « ;j » dans la fonction « matches » pour que « ?! » soit accepté par Saxon) -->
    <xsl:param name="graphèmes"/>
    <xsl:variable name="graphèmes_chevauchants">
        <xsl:call-template name="trouver_graphèmes_chevauchants">
            <xsl:with-param name="graphèmes" select="$graphèmes"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$graphèmes_chevauchants != ''">
        <xsl:text>(?!</xsl:text>
        <xsl:for-each select="$graphèmes_chevauchants//Graphème">
            <xsl:value-of select="."/>
            <xsl:if test="not(position() = last())">
                <xsl:text>|</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="trouver_graphèmes_chevauchants">  <!-- exemple : x et xz -->
    <xsl:param name="graphèmes"/>
    <xsl:element name="Graphèmes">
        <xsl:for-each select="//OrdreLexicographique//Élément[not(Élément)]">  <!-- chaque graphème utilisé dans l'ordre lexicographique -->
            <xsl:variable name="graphème_comparé" select="."/>
            <xsl:for-each select="$graphèmes/Élément|$graphèmes[not(Élément)]">  <!-- chaque graphème formant la lettrine (formant un bloc d'entrées) -->
                <xsl:if test="starts-with($graphème_comparé, .) and $graphème_comparé != .">
                    <Graphème>
                        <xsl:value-of select="$graphème_comparé"/>
                    </Graphème>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
