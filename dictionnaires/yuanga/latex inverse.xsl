<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:import href="en-tête.xsl"/>

<xsl:template match="RessourceLexicale">
<xsl:apply-templates select="." mode="inverse"/>
</xsl:template>

<xsl:template match="RessourceLexicale" mode="inverse">
<xsl:call-template name="ajouter_entête_générale"/>
<xsl:call-template name="ajouter_entête_inverse"/>

\begin{document}

\pagenumbering{roman}

<xsl:call-template name="ajouter_premières_pages"/>

\bibliographie

\mainmatter

\begin{dictionnaire}[\textefra{Dictionnaire inverse}]
<xsl:apply-templates select="InformationsLexicographiques" mode="inverse"/>
\end{dictionnaire}

\tabledesmatières

\end{document}

</xsl:template>

<xsl:template match="InformationsLexicographiques" mode="inverse">
    <xsl:apply-templates select="OrdreLexicographiqueInverse" mode="inverse"/>
</xsl:template>

<xsl:template match="OrdreLexicographiqueInverse" mode="inverse">
    <xsl:apply-templates select="Élément" mode="inverse"/>
</xsl:template>

<xsl:template match="Élément" mode="inverse">
    <xsl:variable name="lettrine" select="if (Élément) then string-join(Élément, ' – ') else ." />
    <xsl:variable name="expression_rationnelle_graphèmes">
        <xsl:call-template name="créer_expression_rationnelle_graphèmes_simple">
            <xsl:with-param name="graphèmes" select="."/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bloc_entrées">
        <xsl:apply-templates select="/RessourceLexicale/Dictionnaire//Sens//Glose[matches(replace(., '[\p{P}]', ''), $expression_rationnelle_graphèmes, 'i;j')]" mode="inverse">
            <xsl:sort select="replace(., '[\p{P}]', '')" lang="fr"/>
        </xsl:apply-templates>
    </xsl:variable>
    <xsl:if test="$bloc_entrées != ''">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\begin{bloclettrine}{</xsl:text>
        <xsl:value-of select="$lettrine"/>
        <xsl:text>}</xsl:text>
        <xsl:text>&#10;</xsl:text>
        <xsl:value-of select="$bloc_entrées"/>
        <xsl:text>\end{bloclettrine}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="Glose[ancestor::Sous-entréeLexicale or ancestor::RelationSémantique]" mode="inverse">
</xsl:template>

<xsl:template match="Glose[not(ancestor::Sous-entréeLexicale or ancestor::RelationSémantique) and @langue='fra']" mode="inverse">
    <xsl:text>\begin{entréeinverse}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="node()" mode="inverse"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="ancestor::Sens/PartieDuDiscours" mode="inverse"/>
    <xsl:apply-templates select="ancestor::EntréeLexicale/Lemme/Forme" mode="inverse"/>
    <xsl:apply-templates select="ancestor::EntréeLexicale/NuméroDHomonyme" mode="inverse"/>
    <xsl:apply-templates select="ancestor::EntréeLexicale/Lemme/Région" mode="inverse"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{entréeinverse}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="PartieDuDiscours" mode="inverse">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\partiedudiscours{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Forme" mode="inverse">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\renvoi{</xsl:text>
    <xsl:value-of select="replace(., '_', '')"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="ancestor::EntréeLexicale/@identifiant"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NuméroDHomonyme" mode="inverse">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\numérodhomonyme{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Région" mode="inverse">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\région{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="lien" mode="inverse">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\lien{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(., '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style" mode="inverse">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="text()" mode="inverse">
    <xsl:value-of select="replace(., '&quot;(.+?)&quot;', '« $1 »')"/>
</xsl:template>

<xsl:template name="créer_expression_rationnelle_graphèmes_simple">  <!-- forme : ^(x|y|z) -->
    <xsl:param name="graphèmes"/>
    <xsl:text>^(</xsl:text>
    <xsl:choose>
        <xsl:when test="Élément">
            <xsl:for-each select="$graphèmes/Élément">
                <xsl:value-of select="."/>
                <xsl:if test="not(position() = last())">
                    <xsl:text>|</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$graphèmes"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>)</xsl:text>
</xsl:template>

</xsl:stylesheet>
