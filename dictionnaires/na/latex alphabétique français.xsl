<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:import href="général.xsl"/>

<xsl:variable name="languedocument">fra</xsl:variable>
<xsl:variable name="ordrelangues" select="['nru', 'fra', 'cmn']"/>
<xsl:variable name="ordrelanguesnotes" select="['fra']"/>
<xsl:variable name="fichierintroduction">introduction française.tex</xsl:variable>

<xsl:variable name="couleurdéf">Gray</xsl:variable>
<xsl:variable name="couleurnru">Blue</xsl:variable>
<xsl:variable name="couleurfra">black</xsl:variable>
<xsl:variable name="couleureng">Sepia</xsl:variable>
<xsl:variable name="couleurcmn">OliveGreen</xsl:variable>
<xsl:variable name="couleuremprunt">CadetBlue</xsl:variable>
<xsl:variable name="couleurpartiedudiscours">PineGreen</xsl:variable>
<xsl:variable name="couleurlocuteur">Mahogany</xsl:variable>
<xsl:variable name="couleurdoi">TealBlue</xsl:variable>
<xsl:variable name="couleurréférencetexte">Melon</xsl:variable>
<xsl:variable name="couleurphonologie">Purple</xsl:variable>
<xsl:variable name="couleurphonétique">Emerald</xsl:variable>

<xsl:template name="ajouter_première_de_couverture">
\includepdf[pages=1,fitpaper,trim=36.1cm 1.5cm 11.6cm 1.5cm]{couverture française.pdf}
</xsl:template>

<xsl:template name="ajouter_quatrième_de_couverture">
\includepdf[pages=1,fitpaper,trim=11.6cm 1.5cm 36.1cm 1.5cm]{couverture française.pdf}
</xsl:template>

<xsl:template name="ajouter_préambule_complémentaire">
\hypersetup{pdfauthor={Alexis Michaud, Latami Dashilamu, Pascale-Marie Milan, Benjamin Galliot}, pdfkeywords={dictionnaire, Collection Lexica, Lexika, na, mosuo, nru}, pdfsubject={Dictionnaire na (mosuo) – chinois – français}, pdftitle={Dictionnaire na (mosuo) – chinois – français}}

\RenewDocumentEnvironment {gloses} { } {\unskip\pfra{\pdéf{◈} En dialecte chinois local :}}{\ignorespacesafterend}
\RenewDocumentEnvironment {exemple} {} {\unskip\enskip\pfradéf{¶}\:\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {variantes} {} {\unskip\enskip\pfradéf{(}\ignorespaces} {\unskip\pfradéf{)}\ignorespacesafterend}
\NewDocumentEnvironment {cognats} {} {\unskip\enskip\pdéf{🟔} \pfra{Cognats : }\ignorespaces}{\ignorespacesafterend}
\NewDocumentEnvironment {comparanda} {} {\unskip\enskip\pdéf{🟔} \pfra{Comparanda : }\ignorespaces}{\ignorespacesafterend}
\RenewDocumentEnvironment {usages} {} {\unskip\enskip\pdéf{🟔} \pfra{Usage : }\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {classificateurs} { O{1} } {\unskip\enskip\pdéf{🟔} \pfra{Classificateur\ifnum#1&gt;1 s\fi: }\ignorespaces} {\scriptcjc\ignorespacesafterend}
\RenewDocumentEnvironment {morphologie} { } {\unskip\enskip\pdéf{🟔} \pfra{Composition morphologique : }\ignorespaces}{\scriptcjc\ignorespacesafterend}
\RenewDocumentEnvironment {étymologie} {} {\unskip\enskip\pdéf{🟔} \pfra{Étymologie : }\ignorespaces}{\ignorespacesafterend}
\NewDocumentCommand \ton { m } {\unskip\enskip\pdéf{🟔} \pfra{Ton : }\papi{#1}}
\RenewDocumentCommand \partiedudiscours { m } {\pfra{\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{\textsc{#1}}}}
\RenewDocumentCommand \variante { m m } {\pfra{#2 : \pnru{#1}}}
\RenewDocumentCommand \usage { m } {\pfra{#1}}
\NewDocumentCommand \notegénérale { m }{\typenote{\pfra{(#1)}}}
\RenewDocumentCommand \relationsémantique { m m } {\unskip\enskip\pdéf{▣} \pfra{#1 : }\pnru{#2}}
\RenewDocumentCommand \emprunt { m m } {\pfra{(emprunt #2) #1}}
\NewDocumentCommand \cognat { m m } {\pfra{(#2)} \pnru{#1}}
\NewDocumentCommand \comparandum { m m } {\pfra{(#2)} \pnru{#1}}
\RenewDocumentCommand \classificateur { m m } {#1 #2}
\RenewDocumentCommand \champclassificateur { m } {\pfra{(#1)}}
\NewDocumentCommand \séparateurénumération { } {\pfra{, }}

\RenewExpandableDocumentCommand \nomdictionnaire {} {Dictionnaire}
\NewExpandableDocumentCommand \nombibliographie {} {Bibliographie}
\NewExpandableDocumentCommand \nomtabledesmatières {} {Table des matières}
\NewExpandableDocumentCommand \nompostface {} {Postface}

\RenewDocumentCommand \lstlistingname {} {Code}

\DefTblrTemplate{contfoot-text}{default}{Suite page suivante…}
\DefTblrTemplate{conthead-text}{default}{(suite)}
</xsl:template>

<xsl:template name="ajouter_premières_pages">
\input{variables}

\pagedetitre{\titrepremièrefra}

\pagedesinformations[\languefra\small]{
    titre = {\titrefra},
    linguiste = {\alexisfra{} (\textecmncn{\alexiscmn})},
    rôleslinguiste = {collecte et compilation des données, recherche linguistique et rédaction},
    concepteur = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    rôlesconcepteur = {conception et ingénierie lexicographiques et artistiques, développement informatique, conception de la couverture et rédaction de la postface},
    autresauteurs = {\item \dashilamufra{} (\textecmncn{\dashilamucmn}) : connaisseuse de la langue et de la tradition orale na, locutrice de référence et collaboratrice dans l'entreprise de documentation ; \item \pascalemariefra{} (\textecmncn{\pascalemariecmn}) : recherche anthropologique et rédaction ;},
    contributeurs = {\item \rosellefra{} (\textecmncn{\rosellecmn}) : transcription orthographique ; \item \wangyongfra{} (\textecmncn{\wangyongcmn}) : consultant éditorial pour la version chinoise.},
    directeurcollection = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    responsablescientifique = {\guillaumefra{} (\textecmncn{\guillaumecmn})},
    codesource = {\url{https://gitlab.com/BenjaminGalliot/Lexika/-/tree/maîtresse/dictionnaires/na}},
    identifiantHAL = {halshs-01204645v4},
    numéro = {3},
    version = \version,
    polices = {
        \begin{itemize}
            \item Script latin : EB Garamond 12\\\url{https://github.com/octaviopardo/EBGaramond12}
            \item Script CJCV (chinois-japonais-coréen-vietnamien) :
                \begin{itemize}
                    \item STKaiti (\textecmncn{华文楷体})\\\emph{Gracieusement fournie par Sinotype, que nous remercions pour leur soutien.}\\Licence n\up{o} 202407000001003-000.\\\url{https://sinotype.vcg.com}
                    \item King Hwa Old Song (\textecmncn{京華老宋体})\\\url{https://zfont.cn/cn/font_579.html}
                \end{itemize}
            \item API : Gentium Plus\\\url{https://software.sil.org/gentium}
            \item Symboles divers : Noto Sans Symbols 2\\\url{https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2}
        \end{itemize}
    },
    divers = {Photos de couverture réalisées par \alexisfra{} (\textecmncn{\alexiscmn}) en 2007.\\Traduction des informations en chinois par \textsc{Yin} Yuanhao (\textecmncn{殷元昊}).}
}[\medskipamount]

\clearpage

\pagedetitrecomplète{\titrepremièrefra}{
    \textbf{\plettresstylisées{\alexisfra}}（\textbf{\textecmncn{\alexiscmn}}）

    \textbf{\plettresstylisées{\dashilamufra}}（\textbf{\textecmncn{\dashilamucmn}}）

    \textbf{\plettresstylisées{\pascalemariefra}}（\textbf{\textecmncn{\pascalemariecmn}}）

    \bigskip

    \textegrisé{Conception et ingénierie lexicographiques et artistiques}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）

    \bigskip

    \textegrisé{Transcription orthographique}

    \textbf{\plettresstylisées{\rosellefra}}（\textbf{\textecmncn{\rosellecmn}}）
}[
    \collectionlexicafra
]{
    \textegrisé{Directeur de collection et responsable informatique}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）

    \bigskip

    \textegrisé{Responsable scientifique}

    \textbf{\plettresstylisées{\guillaumefra}}（\textbf{\textecmncn{\guillaumecmn}}）
}

\pagedecollection
</xsl:template>

<xsl:template name="ajouter_épigraphe">
\pagedépigraphe{
    \pnru{se˩˥ ◊ -dʑo˩, | se˩-mɤ˩-tʰɑ˩˥! | dʑɤ˩˥ ◊ -dʑo˩, | dʑɤ˩-kʰɯ˧ tʰɑ˥!}\\
    \orthographeexemple{Seiq jjo, seiq meta! Jjaq jjo, jjaq keeq ta!}\\
    \pfra{On n'en verra jamais le bout. Pour autant, on peut tout de même arriver à faire quelque chose de bien!}
}
</xsl:template>

<xsl:template name="ajouter_préface">
<!-- \begin{préface}
    \input{préface française.tex}
\end{préface} -->
</xsl:template>

<xsl:template name="ajouter_postface">
\begin{postface}
    \input{postface française.tex}
\end{postface}
</xsl:template>

<xsl:template match="/">
    <xsl:apply-templates select="RessourceLexicale"/>
</xsl:template>

</xsl:stylesheet>
