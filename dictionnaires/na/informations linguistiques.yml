généralités:
    modèles:
        ligne: "^\\\\(?<balise>\\w*)\\s*(<(?<métadonnées>.+?)>)?\\s*(?<données>.*)"
        métadonnées: "(?<clef>\\w+)=\"(?<valeur>\\w+)\""
        nom d’abstraction: "^(?<nom>[\\w\\s’-]+)(?=$|(\\s(?<arguments>.+?))$)"
        texte enrichi:
          - "(?<ensemble>\\|(?<style>fv|fg|fn|fi|fo|tib){(?<texte>.+?)})"
    substitutions:
        texte enrichi:
          - "<style type=\"\\g<style>\">\\g<texte></style>"

balises:
    # Balises ignorées (notes).
    sf: ~  # À mettre éventuellement plus tard dans une note non affichable.

    # Balises liées aux entrées.
    ton:
        abstractions:
          - nom: ton

    or:
        abstractions:
          - nom: orthographe

    mr:
        abstractions:
          - nom: morphème

    mc:
        abstractions:
          - nom: note de morphème
            caractéristiques:
              - nom: langue
                champ: langue  # +
                traitement: métadonnée  # +

    lc:
        abstractions:
          - nom: forme de surface

    va:
        abstractions:
          - nom: variante
          - nom: locuteur
            champ: speaker  # ≠
            valeur par défaut: inconnu
            traitement: "métadonnée : variante"  # +

    vc:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                champ: langue  # +
                traitement: métadonnée  # +
          - nom: type de note
            champ: type  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +
          - nom: domaine de note
            champ: dom  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +

    # Balises liées aux gloses.
    gn:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 3  # ≠
                traitement: code linguistique

    gf:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    gr:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 4
                traitement: code linguistique

    # Balises liées aux définitions.
    dn:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: cible 3  # ≠
                traitement: code linguistique

    df:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    # Balises liées aux exemples.
    xn:
        abstractions:
          - nom: traduction d’exemple
            caractéristiques:
              - nom: langue
                valeur: cible 3  # ≠
                traitement: code linguistique

    xf:
        abstractions:
          - nom: traduction d’exemple
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    xc:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                champ: langue  # +
                traitement: métadonnée  # +
          - nom: type de note
            champ: type  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +
          - nom: domaine de note
            champ: dom  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +

    xo:
        abstractions:
          - nom: orthographe

    txt:
        abstractions:
          - nom: référence d’exemple
            caractéristiques:
              - nom: type
                valeur: texte

    # Balises liées aux références.
    rf:
        abstractions:
          - nom: référence d’exemple
            caractéristiques:
              - nom: type  # +
                valeur: locuteur  # +

    doi:
        abstractions:
          - nom: référence d’exemple
            caractéristiques:
              - nom: type
                valeur: doi

    txtsup:
        abstractions:
          - nom: référence d’exemple supplémentaire
            caractéristiques:
              - nom: type
                valeur: texte

    doisup:
        abstractions:
          - nom: référence d’exemple supplémentaire
            caractéristiques:
              - nom: type
                valeur: doi

    # Balises liées aux notes.
    nt:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                champ: langue  # +
                valeur par défaut: inconnue   # +
                traitement: métadonnée  # +
          - nom: type de note
            champ: type  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +
          - nom: domaine de note
            champ: dom  # +
            valeur par défaut: ~  # +
            traitement: "métadonnée : note"  # +

    # Balises liées aux usages.
    ue:
        abstractions:
          - nom: usage
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    # Balises liées aux classificateurs.
    pdv:
        abstractions:
          - nom: forme de classificateur

    pdc:
        abstractions:
          - nom: champ d’application de classificateur

    # Balises liées aux étymologies.
    bw:
        abstractions:
          - nom: emprunt
            caractéristiques:
              - nom: langue
                champ: langue  # +
                valeur par défaut: inconnue  # +
                traitement: métadonnée  # +
          - nom: langue d’étymon
            champ: langue  # +
            valeur par défaut: inconnue  # +
            traitement: "métadonnée : ~"  # +

    cog:
        abstractions:
          - nom: cognat
          - nom: langue d’étymon
            champ: langue  # +
            valeur par défaut: inconnue  # +
            traitement: "métadonnée : cognat"  # +

    comp:
        abstractions:
          - nom: comparandum
          - nom: langue d’étymon
            champ: langue  # +
            valeur par défaut: inconnue  # +
            traitement: "métadonnée : comparandum"  # +

abstractions:
    # Abstractions liées aux entrées.
    liste d’entrées lexicales:
        entité:
            nom: entrées lexicales
        descendants:
          - nom: entrée lexicale <(graphèmes, tons, chiffres, symboles)  # ≠

    entrée lexicale:
        arguments: "¶"
        entité:
            nom: entrée lexicale
        descendants:
          - nom: numéro d’homonyme [1]
          - nom: lemme [1]
          - nom: morphologie [1]
          - nom: liste de groupes [1]
          - nom: partie du discours [1]
          - nom: liste de notes [1]
          - nom: liste de formes grammaticales [1]
          - nom: liste de paradigmes [1]
          - nom: liste de renvois inverses [1]
          - nom: liste de sens [1]
          - nom: liste de sous-entrées lexicales [1]
          - nom: étymologie [1]
          - nom: comparatisme [1]  # +
          - nom: multimédia [1]
          - nom: date [1]

    lemme:
        entité:
            nom: lemme
        descendants:
          - nom: vedette [1]
          - nom: ton [1]  # +
          - nom: orthographe [1]  # +
          - nom: forme de surface [1]  # +
          - nom: liste de variantes [1]
          - nom: forme phonétique [1]
          - nom: forme de citation [1]

    ton:
       entité:
           nom: ton

    orthographe:
       entité:
           nom: orthographe

    forme de surface:
       entité:
           nom: forme de surface

    liste de variantes:
        entité:
            nom: variantes
        descendants:
          - nom: bloc de variante

    bloc de variante:  # +
        entité:
            nom: variante
        descendants:
          - nom: variante [1]
          - nom: liste de notes [1]  # +
          - nom: locuteur [1]  # +

    variante:
        entité:
            nom: forme

    locuteur:
        entité:
            nom: locuteur

    # Abstractions liées aux sens.
    sens:
        entité:
            nom: sens
        descendants:
          - nom: numéro de sens [1]
          - nom: liste de domaines sémantiques [1]
          - nom: liste d’index de domaines sémantiques [1]
          - nom: liste de notes [1]  # +
          - nom: sens littéral [1]
          - nom: liste de définitions [1]
          - nom: liste de gloses [1]
          - nom: liste de gloses interlinéaires [1]
          - nom: liste d’usages [1]
          - nom: liste d’exemples [1]
          - nom: liste de relations sémantiques [1]
          - nom: liste de restrictions sémantiques [1]
          - nom: liste de paradigmes [1]
          - nom: liste de classificateurs [1]
          - nom: liste d’informations encyclopédiques [1]
          - nom: liste de noms scientifiques [1]
          - nom: multimédia [1]

    liste de définitions:
        entité:
            nom: définitions
        descendants:
          - nom: définition  # ≠

    bloc de définition: ~

    définition:
        arguments: "^"
        entité:
            nom: définition  # ≠

    liste d’exemples:
        entité:
            nom: exemples
        descendants:
          - nom: bloc d’exemple
          - nom: bloc d’exemple supplémentaire  # +

    bloc d’exemple:
        entité:
            nom: exemple
        descendants:
          - nom: exemple [1]
          - nom: orthographe [1]
          - nom: traduction d’exemple
          - nom: liste de notes [1]
          - nom: références d’exemple [1]

    bloc d’exemple supplémentaire:
        entité:
            nom: exemple
        descendants:
          - nom: références d’exemple supplémentaire [1]

    références d’exemple:
        entité:
            nom: références
        descendants:
          - nom: référence d’exemple

    références d’exemple supplémentaire:
        entité:
            nom: références
        descendants:
          - nom: référence d’exemple supplémentaire

    référence d’exemple:
        entité:
            nom: référence

    référence d’exemple supplémentaire:
        arguments: "^"
        entité:
            nom: référence

    liste d’usages:
        entité:
            nom: usages
        descendants:
          - nom: usage  # ≠

    bloc d’usage: ~

    usage:
        entité:
            nom: usage  # ≠

    # Abstractions liées aux classificateurs.
    forme de classificateur:
        arguments: "@(entrée lexicale, sous-entrée lexicale)"  # +
        entité:
            nom: forme

    # Abstractions liées à la morphologie.
    morphologie:
        entité:
            nom: morphologie
        descendants:
          - nom: morphème  # +
          - nom: liste de notes de morphème [1]  # +

    morphème:
        arguments: "@(entrée lexicale, sous-entrée lexicale)"
        entité:
            nom: morphème

    # Abstractions liées aux étymologies.
    étymologie:
        entité:
            nom: étymologie
        descendants:
          - nom: liste d’étymons
          - nom: bloc d’emprunt

    bloc d’étymon: ~

    bloc d’emprunt:
        entité:
            nom: emprunt
        descendants:
          - nom: emprunt [1]
          - nom: langue d’étymon [1]

    emprunt:
        entité:
            nom: forme  # ≠

    # Abstractions liées aux comparatismes.
    comparatisme:
        entité:
            nom: comparatisme
        descendants:
          - nom: liste de cognats
          - nom: liste de comparanda
          - nom: liste de notes [1]  # +

    liste de cognats:
        entité:
            nom: cognats
        descendants:
          - nom: bloc de cognat

    bloc de cognat:
        entité:
            nom: cognat
        descendants:
          - nom: cognat [1]
          - nom: langue d’étymon [1]

    cognat:
        entité:
            nom: forme

    liste de comparanda:
        entité:
            nom: comparanda
        descendants:
          - nom: bloc de comparandum

    bloc de comparandum:
        entité:
            nom: comparandum
        descendants:
          - nom: comparandum [1]
          - nom: langue d’étymon [1]

    comparandum:
        entité:
            nom: forme

    # Abstractions liées aux notes.
    bloc de note:
        entité:
            nom: note
        descendants:
          - nom: note [1]
          - nom: type de note [1]
          - nom: domaine de note [1]

    domaine de note:
        entité:
            nom: domaine

    liste de notes de morphème:
        entité:
            nom: notes
        descendants:
          - nom: bloc de note de morphème

    bloc de note de morphème:
        entité:
            nom: note
        descendants:
          - nom: note de morphème [1]

    note de morphème:
        arguments: "^"
        entité:
            nom: texte