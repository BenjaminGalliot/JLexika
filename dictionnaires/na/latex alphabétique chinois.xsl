<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:import href="général.xsl"/>

<xsl:variable name="languedocument">cmn</xsl:variable>
<xsl:variable name="ordrelangues" select="['nru', 'cmn', 'eng', 'fra']"/>
<xsl:variable name="ordrelanguesnotes" select="['cmn', 'eng', 'fra']"/>
<xsl:variable name="fichierintroduction">introduction chinoise.tex</xsl:variable>

<xsl:variable name="couleurdéf">Gray</xsl:variable>
<xsl:variable name="couleurnru">Blue</xsl:variable>
<xsl:variable name="couleurfra">OliveGreen</xsl:variable>
<xsl:variable name="couleureng">Sepia</xsl:variable>
<xsl:variable name="couleurcmn">black</xsl:variable>
<xsl:variable name="couleuremprunt">CadetBlue</xsl:variable>
<xsl:variable name="couleurpartiedudiscours">PineGreen</xsl:variable>
<xsl:variable name="couleurlocuteur">Mahogany</xsl:variable>
<xsl:variable name="couleurdoi">TealBlue</xsl:variable>
<xsl:variable name="couleurréférencetexte">Melon</xsl:variable>
<xsl:variable name="couleurphonologie">Purple</xsl:variable>
<xsl:variable name="couleurphonétique">Emerald</xsl:variable>

<xsl:template name="ajouter_première_de_couverture">
\includepdf[pages=1,fitpaper,trim=36.1cm 1.5cm 11.6cm 1.5cm]{couverture chinoise.pdf}
</xsl:template>

<xsl:template name="ajouter_quatrième_de_couverture">
\includepdf[pages=1,fitpaper,trim=11.6cm 1.5cm 36.1cm 1.5cm]{couverture chinoise.pdf}
</xsl:template>

<xsl:template name="ajouter_préambule_complémentaire">
\hypersetup{pdfauthor={Alexis Michaud/米可, Latami Dashilamu/拉他咪打史拉姆, Pascale-Marie Milan/皮艾木, Benjamin Galliot/區勄狐}, pdfkeywords={词典, Collection Lexica/世界珍稀语言系列词典丛书, Lexika, 摩梭语, nru}, pdfsubject={永宁阿拉瓦村摩梭方言—汉—英—法词典}, pdftitle={永宁阿拉瓦村摩梭方言—汉—英—法词典}}

\RenewDocumentEnvironment {gloses} { } {\unskip\pcmn{\pdéf{◈}当地汉语方言：}}{\ignorespacesafterend}
\RenewDocumentEnvironment {exemple} {} {\unskip\enskip\pfradéf{¶}\:\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {variantes} {} {\unskip\pcmn{（}\ignorespaces} {\unskip\pcmn{）}\ignorespacesafterend}
\NewDocumentEnvironment {cognats} {} {\unskip\pcmn{【同源词】}\ignorespaces}{\ignorespacesafterend}
\NewDocumentEnvironment {comparanda} {} {\unskip\pcmn{【比较对象】}\ignorespaces}{\ignorespacesafterend}
\RenewDocumentEnvironment {usages} {} {\unskip\pcmn{【用法】}\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {classificateurs} { O{1} } {\unskip\pcmn{【量词】}\ignorespaces\scriptlatin} {\scriptcjc\ignorespacesafterend}
\RenewDocumentEnvironment {morphologie} { } {\unskip\pcmn{【形态构成】}\ignorespaces\scriptlatin}{\scriptcjc\ignorespacesafterend}
\RenewDocumentEnvironment {étymologie} {} {\unskip\pcmn{【词源】}\ignorespaces}{\ignorespacesafterend}
\NewDocumentCommand \ton { m } {\unskip\enskip\pcmn{本调：}\papi{#1}}
\RenewDocumentCommand \partiedudiscours { m } {\pcmn{\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{#1}}}
\RenewDocumentCommand \variante { m m } {\pcmn{#2：\pnru{#1}}}
\RenewDocumentCommand \usage { m } {\unskip\pcmn{#1}}
\NewDocumentCommand \notegénérale { m }{\typenote{\pcmn{（#1）}}}
\RenewDocumentCommand \relationsémantique { m m } {\unskip\pcmn{【#1】}\pnru{#2}}
\RenewDocumentCommand \emprunt { m m } {\pcmn{（#2借词）#1}}
\NewDocumentCommand \cognat { m m } {\unskip\pcmn{（#2）}\pnru{#1}}
\NewDocumentCommand \comparandum { m m } {\unskip\pcmn{（#2）}\pnru{#1}}
\RenewDocumentCommand \classificateur { m m } {#1#2}
\RenewDocumentCommand \champclassificateur { m } {\pcmn{（#1）}}
\NewDocumentCommand \séparateurénumération { } {\pcmn{、}}

\RenewDocumentCommand \stylenomchapitre {} {\Huge}
\RenewDocumentCommand \styletitrechapitre {} {\Huge\upshape\scshape}

\RenewExpandableDocumentCommand \nomdictionnaire {} {\foreignlanguage{cmncn}{词典}}
\NewExpandableDocumentCommand \nombibliographie {} {\foreignlanguage{cmncn}{参考文献}}
\NewExpandableDocumentCommand \nomtabledesmatières {} {\foreignlanguage{cmncn}{目录}}
\NewExpandableDocumentCommand \nompréface {} {序言}
\NewExpandableDocumentCommand \nompostface {} {跋文}

\RenewDocumentCommand \lstlistingname {} {代码}
<!-- \RenewDocumentCommand \contentsname {} {} -->

\DefTblrTemplate{contfoot-text}{default}{续下页}
\DefTblrTemplate{conthead-text}{default}{（续）}

\RenewDocumentCommand \mkbibparens { m } {（#1）}
\RenewDocumentCommand \bibopenparen { m } {\unskip（}
\RenewDocumentCommand \bibcloseparen { m } {）\ignorespaces} % Insuffisant pour les cas compliqués.

\DeclareSourcemap{
    \maps[datatype=bibtex]{
        \map[overwrite]{
        \step[fieldsource=shortauthorzh, fieldtarget=shortauthor]
        }
    }
}

\ExplSyntaxOn
\makeatletter
\RenewDocumentCommand \blocdescontributeurs {} {
    著者：
    \begin{itemize}
        \item \cmdKV@pagedesinformations@linguiste：\cmdKV@pagedesinformations@rôleslinguiste
        \str_if_eq:VnF{\cmdKV@pagedesinformations@autresauteurs} {} {
            \cmdKV@pagedesinformations@autresauteurs
        }
        \item \cmdKV@pagedesinformations@concepteur：\cmdKV@pagedesinformations@rôlesconcepteur
    \end{itemize}

    \str_if_eq:VnF~{\cmdKV@pagedesinformations@contributeurs} {} {
        \vspace*{\medskipamount}
        特别致谢：
        \begin{itemize}
            \cmdKV@pagedesinformations@contributeurs
        \end{itemize}
    }
}

\RenewDocumentCommand \pagedesinformations { O{\languecmncn} m } {
    \setkeys{pagedesinformations}{#2}
    \clearpage
    \pagestyle{informations}
    \noindent
    {
        #1

        书名：\cmdKV@pagedesinformations@titre\\[\medskipamount]

        \blocdescontributeurs

        \vspace*{\medskipamount}

        版本信息：
        \begin{itemize}
            \item 版本号：\str_if_eq:VnF {\cmdKV@pagedesinformations@version} {} {\textefra{\cmdKV@pagedesinformations@version}}
            \item 日期：\textefra{\cmdKV@pagedesinformations@datelivre}
            \item \collectionlexicacmn 编号：第\textefra{\cmdKV@pagedesinformations@numéro} 卷
        \end{itemize}

        \vspace*{\medskipamount}

        \collectionlexicacmn（\textefra{\collectionlexicafra}）：
        \begin{itemize}
            \item \cmdKV@pagedesinformations@directeurcollection：技术主编
            \item \cmdKV@pagedesinformations@responsablescientifique：内容主编
        \end{itemize}
        \url{https://shs.hal.science/LEXICA}\\[\medskipamount]

        开放访问与版权信息：本书基于\textefra{\emph{Creative~Commons~Attribution~4.0}}协议发布（署名—非商业性使用—相同方式共享\textefra{4.0}协议国际版）。\\
        \url{https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh-hans}\\[\medskipamount]

        可下载地址：\\
        \url{https://shs.hal.science/\cmdKV@pagedesinformations@identifiantHAL}\\[\medskipamount]

        源码地址：\\
        \cmdKV@pagedesinformations@codesource\\[\medskipamount]

        所用软件：
        \begin{itemize}
            \item 词典建模及设计软件：\textefra{Lexika}（由\benjamincmn（\benjaminfra）设计开发）\\
            \url{https://gitlab.com/BenjaminGalliot/Lexika}
            \item 文档排版软件及语言：\textefra{\LuaLaTeX}\\
            \url{http://wiki.luatex.org/index.php}
        \end{itemize}

        \vspace*{\medskipamount}

        所用字体：
        \cmdKV@pagedesinformations@polices

        \str_if_eq:VnF {\cmdKV@pagedesinformations@divers} {} {
            \vspace*{\medskipamount}
            \cmdKV@pagedesinformations@divers
        }
    }
} {\ignorespacesafterend}

\RenewDocumentCommand \pagedecollection { O{\languecmn} } {
    \clearpage
    #1
    \thispagestyle{collection}
    \vspace*{\topmargin+\complémentinversenombredor\paperheight}
    完成的词典：
    \begin{enumerate}
        \item 嘉绒–汉–法词典
        \item \textefra{Zuanga-yuanga}–法词典
        \item \titrecmn
    \end{enumerate}
}
\makeatother
\ExplSyntaxOff
</xsl:template>

<xsl:template name="ajouter_premières_pages">
\input{variables}

\pagedetitre[\languecmncn]{\titrecmn}

\pagedesinformations[\languecmncn\small]{
    titre = {\titrecmn},
    linguiste = {\alexiscmn（\textefra{\alexisfra}）},
    rôleslinguiste = {负责数据收集、语言学研究与编撰工作},
    concepteur = {\benjamincmn（\textefra{\benjaminfra}）},
    rôlesconcepteur = {负责词典设计与排版技术支持},
    autresauteurs = {\item \dashilamucmn（\textefra{\dashilamufra}）：摩梭语母语者、摩梭语言与口传文化鉴赏家、摩梭语言调查标准发音人与主要合作人\item \pascalemariecmn（\textefra{\pascalemariefra}）：负责人类学研究与编撰工作},
    contributeurs = {\item \rosellecmn（\textefra{\rosellefra}）：拼音转录\item \wangyongcmn（\textefra{\wangyongfra}）：审校},
    directeurcollection = {\benjamincmn（\textefra{\benjaminfra}）},
    responsablescientifique = {\guillaumecmn（\textefra{\guillaumefra}）},
    codesource = {\url{https://gitlab.com/BenjaminGalliot/Lexika/-/tree/maîtresse/dictionnaires/na}},
    identifiantHAL = {halshs-01744420v2},
    numéro = {3},
    version = \version,
    polices = {
        \begin{itemize}
            \item 拉丁字母字体：\textefra{EB Garamond 12}\\\url{https://github.com/octaviopardo/EBGaramond12}
            \item 汉字字体：
                \begin{itemize}
                    \item 华文楷体（\textefra{STKaiti}）\\本字体由华文时空文字技术有限公司无偿提供，特此致以衷心感谢。\\许可证编号：\textefra{202407000001003-000}\\\url{https://sinotype.vcg.com}
                    \item 京華老宋体 (\textefra{King Hwa Old Song})\\\url{https://zfont.cn/cn/font_579.html}
                \end{itemize}
            \item 国际音标字体：\textefra{Gentium Plus}\\\url{https://software.sil.org/gentium}
            \item 其他符号字体：\textefra{Noto Sans Symbols 2}\\\url{https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2}
        \end{itemize}
    },
    divers = {封面设计：\benjamincmn\quad\quad\quad 封面图片：\alexiscmn（\textefra{2007}年）拍摄\\本书信息中文翻译：殷元昊}
}

\clearpage

\pagedetitrecomplète[\languecmncn]{\titrecmn}{
    \textbf{\alexiscmn}（\textbf{\textefra{\plettresstylisées{\alexisfra}}}）

    \textbf{\dashilamucmn}（\textbf{\textefra{\plettresstylisées{\dashilamufra}}}）

    \textbf{\pascalemariecmn}（\textbf{\textefra{\plettresstylisées{\pascalemariefra}}}）

    \bigskip

    \textegrisé{词典设计}

    \textbf{\benjamincmn}（\textbf{\textefra{\plettresstylisées{\benjaminfra}}}）

    \bigskip

    \textegrisé{拼音文字}

    \textbf{\rosellecmn}（\textbf{\textefra{\plettresstylisées{\rosellefra}}}）

    \bigskip

    \textegrisé{审校}

    \textbf{\wangyongcmn}（\textbf{\textefra{\plettresstylisées{\wangyongfra}}}）
}[
    \collectionlexicacmn
]{
    \textegrisé{技术主编}

    \textbf{\benjamincmn}（\textbf{\textefra{\plettresstylisées{\benjaminfra}}}）

    \bigskip

    \textegrisé{内容主编}

    \textbf{\guillaumecmn}（\textbf{\textefra{\plettresstylisées{\guillaumefra}}}）
}

\pagedecollection[\languecmncn]
</xsl:template>

<xsl:template name="ajouter_épigraphe">
\pagedépigraphe[\languecmncn]{
    \pnru{se˩˥ ◊ -dʑo˩, | se˩-mɤ˩-tʰɑ˩˥! | dʑɤ˩˥ ◊ -dʑo˩, | dʑɤ˩-kʰɯ˧ tʰɑ˥!}\\
    \orthographeexemple{Seiq jjo, seiq meta! Jjaq jjo, jjaq keeq ta!}\\
    \pcmn{没有办法做完。不过，还是可以做得很漂亮吧！}
}
</xsl:template>

<xsl:template name="ajouter_préface">
\begin{préface}[\nompréface][]
    \input{préface chinoise.tex}
\end{préface}
</xsl:template>

<xsl:template name="ajouter_postface">
\begin{postface}[\nompostface][]
    \input{postface chinoise.tex}
\end{postface}
</xsl:template>

<xsl:template match="/">
    <xsl:apply-templates select="RessourceLexicale"/>
</xsl:template>

</xsl:stylesheet>
