<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:import href="général.xsl"/>

<xsl:variable name="languedocument">eng</xsl:variable>
<xsl:variable name="ordrelangues" select="['nru', 'eng', 'cmn']"/>
<xsl:variable name="ordrelanguesnotes" select="['eng']"/>
<xsl:variable name="fichierintroduction">introduction anglaise.tex</xsl:variable>

<xsl:variable name="couleurdéf">Gray</xsl:variable>
<xsl:variable name="couleurnru">Blue</xsl:variable>
<xsl:variable name="couleurfra">Sepia</xsl:variable>
<xsl:variable name="couleureng">black</xsl:variable>
<xsl:variable name="couleurcmn">OliveGreen</xsl:variable>
<xsl:variable name="couleuremprunt">CadetBlue</xsl:variable>
<xsl:variable name="couleurpartiedudiscours">PineGreen</xsl:variable>
<xsl:variable name="couleurlocuteur">Mahogany</xsl:variable>
<xsl:variable name="couleurdoi">TealBlue</xsl:variable>
<xsl:variable name="couleurréférencetexte">Melon</xsl:variable>
<xsl:variable name="couleurphonologie">Purple</xsl:variable>
<xsl:variable name="couleurphonétique">Emerald</xsl:variable>

<xsl:template name="ajouter_première_de_couverture">
\includepdf[pages=1,fitpaper,trim=36.1cm 1.5cm 11.6cm 1.5cm]{couverture anglaise.pdf}
</xsl:template>

<xsl:template name="ajouter_quatrième_de_couverture">
\includepdf[pages=1,fitpaper,trim=11.6cm 1.5cm 36.1cm 1.5cm]{couverture anglaise.pdf}
</xsl:template>

<xsl:template name="ajouter_préambule_complémentaire">
\hypersetup{pdfauthor={Alexis Michaud, Latami Dashilamu, Pascale-Marie Milan, Benjamin Galliot}, pdfkeywords={dictionary, Collection Lexica, Lexika, na, mosuo, nru}, pdfsubject={Na (Mosuo) – English – Chinese dictionary}, pdftitle={Na (Mosuo) – English – Chinese dictionary}}

\RenewDocumentEnvironment {gloses} { } {\unskip\peng{\pdéf{◈} In local Chinese dialect:}}{\ignorespacesafterend}
\RenewDocumentEnvironment {exemple} {} {\unskip\enskip\pfradéf{¶}\:\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {variantes} {} {\unskip\enskip\pfradéf{(}\ignorespaces} {\unskip\pfradéf{)}\ignorespacesafterend}
\NewDocumentEnvironment {cognats} {} {\unskip\enskip\pdéf{🟔} \peng{Cognates: }\ignorespaces}{\ignorespacesafterend}
\NewDocumentEnvironment {comparanda} {} {\unskip\enskip\pdéf{🟔} \peng{Comparanda: }\ignorespaces}{\ignorespacesafterend}
\RenewDocumentEnvironment {usages} {} {\unskip\enskip\pdéf{🟔} \peng{Usage: }\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {classificateurs} { O{1} } {\unskip\enskip\pdéf{🟔} \peng{Classifier\ifnum#1&gt;1 s\fi: }\ignorespaces} {\ignorespacesafterend}
\RenewDocumentEnvironment {morphologie} { } {\unskip\enskip\pdéf{🟔} \peng{Morphological makeup: }\ignorespaces\ignorespaces}{\ignorespacesafterend}
\RenewDocumentEnvironment {étymologie} {} {\unskip\enskip\pdéf{🟔} \peng{Etymology: }\ignorespaces}{\ignorespacesafterend}
\NewDocumentCommand \ton { m } {\unskip\enskip\pdéf{🟔} \peng{Tone: }\papi{#1}}
\RenewDocumentCommand \partiedudiscours { m } {\peng{\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{\textsc{#1}}}}
\RenewDocumentCommand \variante { m m } {\peng{#2: \pnru{#1}}}
\RenewDocumentCommand \usage { m } {\peng{#1}}
\NewDocumentCommand \notegénérale { m }{\typenote{\peng{(#1)}}}
\RenewDocumentCommand \relationsémantique { m m } {\unskip\enskip\pdéf{▣} \peng{#1: }\pnru{#2}}
\RenewDocumentCommand \emprunt { m m } {\peng{(#2 loanword) #1}}
\NewDocumentCommand \cognat { m m } {\peng{(#2)} \pnru{#1}}
\NewDocumentCommand \comparandum { m m } {\peng{(#2)} \pnru{#1}}
\RenewDocumentCommand \classificateur { m m } {#1 #2}
\RenewDocumentCommand \champclassificateur { m } {\peng{(#1)}}
\NewDocumentCommand \séparateurénumération {} {\peng{, }}

\RenewExpandableDocumentCommand \nomdictionnaire {} {Dictionary}
\NewExpandableDocumentCommand \nombibliographie {} {Bibliography}
\NewExpandableDocumentCommand \nomtabledesmatières {} {Table of contents}
\NewExpandableDocumentCommand \nompréface {} {Preface}
\NewExpandableDocumentCommand \nompostface {} {Postface}

\RenewDocumentCommand \lstlistingname {} {Code}

\ExplSyntaxOn
\makeatletter
\RenewDocumentCommand \blocdescontributeurs {} {
    Authors:
    \begin{itemize}
        \item \cmdKV@pagedesinformations@linguiste{}:~\cmdKV@pagedesinformations@rôleslinguiste;
        \str_if_eq:VnF{\cmdKV@pagedesinformations@autresauteurs} {} {
            \cmdKV@pagedesinformations@autresauteurs
        }
        \item \cmdKV@pagedesinformations@concepteur{}:~\cmdKV@pagedesinformations@rôlesconcepteur.
    \end{itemize}

    \str_if_eq:VnF~{\cmdKV@pagedesinformations@contributeurs} {} {
        \vspace*{\medskipamount}
        We~would~like~to~thank:
        \begin{itemize}
            \cmdKV@pagedesinformations@contributeurs
        \end{itemize}
    }
}

\RenewDocumentCommand \pagedesinformations { O{\langueeng} m } {
    \setkeys{pagedesinformations}{#2}
    \clearpage
    \pagestyle{informations}
    \noindent
    {
        #1

        Title:~\cmdKV@pagedesinformations@titre.\\[\medskipamount]

        \blocdescontributeurs

        \vspace*{\medskipamount}

        Version~\str_if_eq:VnF {\cmdKV@pagedesinformations@version} {} {\cmdKV@pagedesinformations@version{}~}of~\cmdKV@pagedesinformations@datelivre.\\[\medskipamount]

        Book~No.~\cmdKV@pagedesinformations@numéro{}~of~the~Lexica~Collection.\\[\medskipamount]

        Lexica~Collection:
        \begin{itemize}
            \item \cmdKV@pagedesinformations@directeurcollection{}:~collection~director~and~IT~supervisor;
            \item \cmdKV@pagedesinformations@responsablescientifique{}:~scientific~supervisor.
        \end{itemize}
        \url{https://shs.hal.science/LEXICA}\\[\medskipamount]

        Published~under~\emph{Creative~Commons~Attribution~4.0~(CC~BY~NC~SA~4.0)}~license:\\
        \url{https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en}\\[\medskipamount]

        This~book~can~be~downloaded~from~the~following~website:\\
        \url{https://shs.hal.science/\cmdKV@pagedesinformations@identifiantHAL}\\[\medskipamount]

        Source~code~available~on:\\
        \cmdKV@pagedesinformations@codesource\\[\medskipamount]

        Dictionary~modelling~and~design~software~used:~Lexika~(designed~by~Benjamin~\textsc{Galliot})\\
        \url{https://gitlab.com/BenjaminGalliot/Lexika}\\[\medskipamount]

        Composition~software~system~(and~language)~used:~\LuaLaTeX\\
        \url{http://wiki.luatex.org/index.php}\\[\medskipamount]

        Fonts~used:
        \cmdKV@pagedesinformations@polices

        \str_if_eq:VnF {\cmdKV@pagedesinformations@divers} {} {
            \vspace*{\medskipamount}
            \cmdKV@pagedesinformations@divers
        }
    }
} {\ignorespacesafterend}

\RenewDocumentCommand \pagedecollection { O{\langueeng} } {
    \clearpage
    #1
    \thispagestyle{collection}
    \vspace*{\topmargin+\complémentinversenombredor\paperheight}
    Dictionaries~from~the~collection~already~realized:
    \begin{enumerate}
        \item Japhug~–~Chinese~–~French.
        \item Yuanga~–~French.
        \item Na~–~Chinese~–~French~–~English.
    \end{enumerate}
}
\makeatother
\ExplSyntaxOff
</xsl:template>

<xsl:template name="ajouter_premières_pages">
\input{variables}

\pagedetitre{\titrepremièreeng}

\pagedesinformations[\langueeng\small]{
    titre = {\titreeng},
    linguiste = {\alexisfra{} (\textecmncn{\alexiscmn})},
    rôleslinguiste = {data collection and compilation, linguistic research and writing},
    concepteur = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    rôlesconcepteur = {lexicographic and artistic design and engineering, IT development, cover design and writing of the postface},
    autresauteurs = {\item \dashilamufra{} (\textecmncn{\dashilamucmn}): connoisseur of the Na language and Na oral traditions, reference language consultant and collaborator in the documentation work; \item \pascalemariefra{} (\textecmncn{\pascalemariecmn}): anthropological research and writing;},
    contributeurs = {\item \rosellefra{} (\textecmncn{\rosellecmn}): orthographic transcription; \item \wangyongfra{} (\textecmncn{\wangyongcmn}): editorial consultant for the Chinese version.},
    directeurcollection = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    responsablescientifique = {\guillaumefra{} (\textecmncn{\guillaumecmn})},
    codesource = {\url{https://gitlab.com/BenjaminGalliot/Lexika/-/tree/maîtresse/dictionnaires/na}},
    identifiantHAL = {halshs-01204638v4},
    numéro = {3},
    version = \version,
    polices = {
        \begin{itemize}
            \item Latin script: EB Garamond 12\\\url{https://github.com/octaviopardo/EBGaramond12}
            \item CJKV script (Chinese-Japanese-Korean-Vietnamese):
                \begin{itemize}
                    \item STKaiti (\textecmncn{华文楷体})\\\emph{Courtesy of Sinotype, whom we thank for their support.}\\License No. 202407000001003-000.\\\url{https://sinotype.vcg.com}
                    \item King Hwa Old Song (\textecmncn{京華老宋体})\\\url{https://zfont.cn/cn/font_579.html}
                \end{itemize}
            \item IPA: Gentium Plus\\\url{https://software.sil.org/gentium}
            \item Various symbols: Noto Sans Symbols 2\\\url{https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2}
        \end{itemize}
    },
    divers = {Cover photos taken by \alexisfra{} (\textecmncn{\alexiscmn}) in 2007.\\Translation of informations in Chinese made by \textsc{Yin} Yuanhao (\textecmncn{殷元昊}).}
}

\clearpage

\pagedetitrecomplète{\titrepremièreeng}{
    \textbf{\plettresstylisées{\alexisfra}}（\textbf{\textecmncn{\alexiscmn}}）

    \textbf{\plettresstylisées{\dashilamufra}}（\textbf{\textecmncn{\dashilamucmn}}）

    \textbf{\plettresstylisées{\pascalemariefra}}（\textbf{\textecmncn{\pascalemariecmn}}）

    \bigskip

    \textegrisé{Lexicographical and artistic design and engineering}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）

    \bigskip

    \textegrisé{Orthographic transcription}

    \textbf{\plettresstylisées{\rosellefra}}（\textbf{\textecmncn{\rosellecmn}}）
}[
    \collectionlexicafra
]{
    \textegrisé{Collection director and IT supervisor}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）

    \bigskip

    \textegrisé{Scientific supervisor}

    \textbf{\plettresstylisées{\guillaumefra}}（\textbf{\textecmncn{\guillaumecmn}}）
}

\pagedecollection
</xsl:template>

<xsl:template name="ajouter_épigraphe">
\pagedépigraphe[\langueeng]{
    \pnru{se˩˥ ◊ -dʑo˩, | se˩-mɤ˩-tʰɑ˩˥! | dʑɤ˩˥ ◊ -dʑo˩, | dʑɤ˩-kʰɯ˧ tʰɑ˥!}\\
    \orthographeexemple{Seiq jjo, seiq meta! Jjaq jjo, jjaq keeq ta!}\\
    \peng{This is not something that can ever be brought to full completion. We can arrive at something nice, though!}
}
</xsl:template>

<xsl:template name="ajouter_préface">
<!-- \begin{préface}[\nompréface][]
    \input{préface anglaise.tex}
\end{préface} -->
</xsl:template>

<xsl:template name="ajouter_postface">
\begin{postface}[\nompostface][]
    \input{postface anglaise.tex}
\end{postface}
</xsl:template>

<xsl:template match="/">
    <xsl:apply-templates select="RessourceLexicale"/>
</xsl:template>

</xsl:stylesheet>
