using EzXML

fichier_xml = "japhug.xml"
dossier_audios = "Lien vers AudioJaphug"
document_xml = fichier_xml |> readxml

nœuds_audios = findall("//Audio", document_xml)
format_entrée = "wav"
format_sortie = "mp3"

fichiers_manquants = String[]
fichiers_malnommés = String[]
fichiers_présents = String[]

function convertir_fichier(chemin)
    chemin_entrée = joinpath(dossier_audios, "$chemin.$format_entrée")
    chemin_sortie = joinpath(dossier_audios, "conversion", "$chemin.$format_sortie")
    commande = `sox $chemin_entrée -C 0 $chemin_sortie`
    run(commande)
end

function analyser_fichier(chemin, qualité)
    noms_potentiels = [
        chemin,
        "8_$chemin"
    ]
    (nom_principal, nom_secondaire) = qualité == "élevée" ? noms_potentiels : reverse(noms_potentiels)
    réussite = true
    nom_complet_principal = "$nom_principal.$format_entrée"
    nom_complet_secondaire = "$nom_secondaire.$format_entrée"
    chemin_principal = joinpath(dossier_audios, nom_complet_principal)
    chemin_secondaire = joinpath(dossier_audios, nom_complet_secondaire)
    if !isfile(chemin_principal)
        if !isfile(chemin_secondaire)
            push!(fichiers_manquants, nom_complet_principal)
        else
            push!(fichiers_malnommés, nom_complet_principal)
        end
        réussite = false
    else
        push!(fichiers_présents, nom_principal)
    end
    return réussite
end

for nœud_audio ∈ nœuds_audios
    qualité_audio = findall("Qualité", nœud_audio)
    chemin_audio = findall("CheminDAccès", nœud_audio)
    if length(qualité_audio) ≠ 1 || length(chemin_audio) ≠ 1
        @error("Problème de nœud : $nœud_audio")
    else
        qualité_audio = (qualité_audio |> only).content
        chemin_audio = (chemin_audio |> only).content
        réussite = if qualité_audio ∈ ["élevée", "faible"]
            analyser_fichier(chemin_audio, qualité_audio)
        else
            @error("Problème de qualité : $nœud_audio")
            false
        end
    end
end

nombre_fichiers = length(fichiers_présents)
Threads.@threads for (position, chemin_fichier) ∈ fichiers_présents |> enumerate |> collect
    convertir_fichier(chemin_fichier)
    # println("conversion $position/$nombre_fichiers : $chemin_fichier")
end

open("fichiers_manquants.txt", "w") do fichier
    liste_fichiers = fichiers_manquants
    texte = join(liste_fichiers, "\n")
    write(fichier, texte)
end

open("fichiers_malnommés.txt", "w") do fichier
    liste_fichiers = fichiers_malnommés
    texte = join(liste_fichiers, "\n")
    write(fichier, texte)
end
