<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:variable name="inclureaudio" select="false()"/>

<xsl:variable name="couleurdéf">black</xsl:variable>
<xsl:variable name="couleurjya">Violet</xsl:variable>
<xsl:variable name="couleurfra">black</xsl:variable>
<xsl:variable name="couleurcmn">black</xsl:variable>
<xsl:variable name="couleurpinyin">Plum</xsl:variable>
<xsl:variable name="couleurlat">black</xsl:variable>
<xsl:variable name="couleurpartiedudiscours">PineGreen</xsl:variable>

<xsl:include href="annexes.xsl"/>

<xsl:template match="RessourceLexicale">
% !TEX program = lualatex

\documentclass[type=book]{dictionnaire lexica}
\RequireVersions{
    *{application}{luaTeX} {0000/00/00 v1.18.0}
    *{package}{ctex} {2022/07/14 v2.5.10}
}
\ConfigurerLangue[main]{fra}{}
\ConfigurerLangue{}{}[jya]
\ConfigurerLangue{api}{}
\ConfigurerLangue{cmncn}{}
\ConfigurerLangue{pinyin}{}
\ConfigurerLangue{lat}{}
\RequirePackage{changepage}
\babelpatterns[jya]{%
<xsl:value-of select="unparsed-text('syllabes.txt', 'UTF-8')"/>}
\addbibresource{bibliographie.bib}
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage{tocloft}
\RenewDocumentCommand \cftsecfont {} {\selectlanguage{jya}}
\RequirePackage[fit]{truncate}
\setmainfont{EBGaramond}
\NewDocumentCommand \pdéf { m } {\textesymbolique{#1}}
\NewDocumentCommand \pjya { m } {\textejya{\textcolor{<xsl:value-of select="$couleurjya"/>}{#1}}}
\NewDocumentCommand \papi { m } {\texteapi{#1}}
\NewDocumentCommand \pfra { m } {\textefra{\textcolor{<xsl:value-of select="$couleurfra"/>}{#1}}}
\NewDocumentCommand \pcmn { m } {\textecmncn{\textcolor{<xsl:value-of select="$couleurcmn"/>}{#1}}}
\NewDocumentCommand \ppinyin { m } {\foreignlanguage{pinyin}{\textcolor{<xsl:value-of select="$couleurpinyin"/>}{#1}}}
\NewDocumentCommand \plat { m } {\textelat{\textcolor{<xsl:value-of select="$couleurlat"/>}{#1}}}
\RenewDocumentCommand \vedetteentête { m } {\pjya{#1}}
\RenewDocumentCommand \vedetteentêtecourte { m } {\truncate{5cm}{\pjya{#1}}}
\RenewDocumentCommand \vedette { m } {\pjya{\textbf{#1}}}
\RenewDocumentCommand \sousvedette { m } {\pjya{\textbf{#1}}}
\NewDocumentCommand \composant { m } {\symbolediamant \pjya{#1}}
\RenewDocumentEnvironment {usages} {} {\unskip\pcmn{【用法】}} {\ignorespacesafterend}
\RenewDocumentEnvironment {étymologie} {} {\unskip\pcmn{【词源】}}{\ignorespacesafterend}
\RenewDocumentEnvironment {nomsscientifiques} {} {\unskip\ignorespaces} {\ignorespacesafterend}
\RenewDocumentCommand \partiedudiscours { m } {\pfra{\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{#1}}}
\RenewDocumentCommand \variante { m } {\textesymbolique{/} \pjya{#1}}
\RenewDocumentCommand \relationsémantique { m m } {\unskip\pcmn{【#1】}\pjya{#2}}
\RenewDocumentCommand \formeparadigmatique { m } {\pjya{#1}}
\RenewDocumentCommand \formegrammaticale { m m m } {\pfra{#1#2} : \pjya{#3}}
\RenewDocumentCommand \étymon { m } {\unskip\papi{#1}}
\RenewDocumentCommand \lienbrisé { m } {\pjya{\textcolor{Red}{#1}}}
\RenewDocumentCommand \stylefv { m } {\pjya{#1}}
\RenewDocumentCommand \stylefn { m } {\pcmn{#1}}
\NewDocumentCommand \symbolediamant {} {\textesymbolique{⬥}}
\setlength{\marginparsep}{1.5em}

\hypersetup{urlcolor=RoyalPurple, linkcolor=Cyan}
\hypersetup{pdfkeywords={dictionnaire, lexica, lexika, japhug, jya}, pdfsubject={Dictionnaire japhug – chinois – français}, pdftitle={Dictionnaire japhug – chinois – français}}
<xsl:call-template name="ajouter_audio"/>

\begin{document}

\input{variables}

\pagedetitre{\titrepremièrefra\\\textecmncn{\titrecmn}}

\pagedesinformations{
    titre = {\titrefra},
    linguiste = {\guillaumefra{} (\textecmncn{\guillaumecmn})},
    rôleslinguiste = {collecte et compilation des données, recherche linguistique et rédaction},
    concepteur = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    autresauteurs = {},
    rôlesconcepteur = {conception et ingénierie lexicographiques et artistiques, développement informatique, conception de la couverture et rédaction de la postface},
    directeurcollection = {\benjaminfra{} (\textecmncn{\benjamincmn})},
    responsablescientifique = {\guillaumefra{} (\textecmncn{\guillaumecmn})},
    codesource = {\url{https://gitlab.com/BenjaminGalliot/Lexika/-/tree/maîtresse/dictionnaires/japhug}},
    identifiantHAL = {halshs-01244860v3},
    numéro = {1},
    polices = {
        \begin{itemize}
            \item Script latin : EB Garamond 12\\\url{https://github.com/octaviopardo/EBGaramond12}
            \item Script CJCV (chinois-japonais-coréen-vietnamien) : STKaiti (\textecmncn{华文楷体})\\\emph{Gracieusement fournie par Sinotype, que nous remercions pour leur soutien.}\\Licence n\up{o} 202407000001003-000.\\\url{https://sinotype.vcg.com}
            \item API : Gentium Plus\\\url{https://software.sil.org/gentium}
            \item Symboles divers : Noto Sans Symbols 2\\\url{https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2}
        \end{itemize}
    },
    divers = {Photos de couverture réalisées par \guillaumefra{} (\textecmncn{\guillaumecmn}) en xxxx.\\Traduction des informations en chinois par \textsc{Yin} Yuanhao (\textecmncn{殷元昊}).\\Nous remercions Raphaëlle Chossenot (chargée d’édition) et Claire Sauttreau (apprentie en édition), qui ont fait partie du projet de la collection Lexica durant l’année 2023.}
}

\clearpage

\pagedetitrecomplète{\titrepremièrefra\\\textecmncn{\titrecmn}}{
    \textbf{\plettresstylisées{\guillaumefra}}（\textbf{\textecmncn{\guillaumecmn}}）

    \bigskip

    \textegrisé{Conception et ingénierie lexicographiques et artistiques}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）
}[
    \collectionlexicafra\\\textecmncn{\collectionlexicacmn}
]{
    \textegrisé{Directeur de collection et responsable informatique}

    \textbf{\plettresstylisées{\benjaminfra}}（\textbf{\textecmncn{\benjamincmn}}）

    \bigskip

    \textegrisé{Responsable scientifique}

    \textbf{\plettresstylisées{\guillaumefra}}（\textbf{\textecmncn{\guillaumecmn}}）
}

\pagedecollection

\pagenumbering{roman}

\begin{introduction}
<xsl:call-template name="ajouter_introduction"/>
\end{introduction}
\nocite{*}
\bibliographie

\mainmatter

\begin{dictionnaire*}
<xsl:apply-templates select="InformationsLexicographiques"/>
\end{dictionnaire*}

\backmatter

\tabledesmatières

\end{document}

</xsl:template>

<xsl:template match="InformationsLexicographiques">
    <xsl:apply-templates select="OrdreLexicographique"/>
</xsl:template>

<xsl:template match="OrdreLexicographique">
    <xsl:apply-templates select="Élément"/>
</xsl:template>

<xsl:template match="Élément">
    <xsl:variable name="lettrine" select="if (Élément) then string-join(Élément, ' – ') else ."/>
    <xsl:variable name="expression_rationnelle_graphèmes">
        <xsl:call-template name="créer_expression_rationnelle_graphèmes">
            <xsl:with-param name="graphèmes" select="."/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bloc_entrées">
        <xsl:apply-templates select="/RessourceLexicale/Dictionnaire/EntréesLexicales">
            <xsl:with-param name="expression_rationnelle_graphèmes" select="$expression_rationnelle_graphèmes"/>
        </xsl:apply-templates>
    </xsl:variable>
    <xsl:if test="$bloc_entrées != ''">
        <xsl:if test="not(position() = 1)">
            <xsl:text>\cleardoublepage</xsl:text>
            <xsl:text>&#10;</xsl:text>
        </xsl:if>
        <xsl:text>\begin{bloclettrine}{</xsl:text>
        <xsl:value-of select="$lettrine"/>
        <xsl:text>}</xsl:text>
        <xsl:text>&#10;&#10;</xsl:text>
        <xsl:value-of select="$bloc_entrées"/>
        <xsl:text>\end{bloclettrine}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="EntréesLexicales">
    <xsl:param name="expression_rationnelle_graphèmes"/>
    <xsl:apply-templates select="EntréeLexicale[matches(replace(Lemme/Forme, '[_-]', ''), $expression_rationnelle_graphèmes, 'i;j')]"/>
</xsl:template>

<xsl:template match="EntréeLexicale">
    <xsl:text>\begin{entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="NuméroDHomonyme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}[</xsl:text>
    <xsl:apply-templates select="Multimédia"/>
    <xsl:text>]</xsl:text>
    <xsl:apply-templates select="Lemme/Variantes"/>
    <xsl:apply-templates select="Groupes"/>
    <xsl:apply-templates select="PartieDuDiscours"/>
    <xsl:apply-templates select="Notes"/>
    <xsl:apply-templates select="Paradigmes"/>
    <xsl:apply-templates select="ListeDeSens"/>
    <xsl:apply-templates select="FormesGrammaticales"/>
    <xsl:apply-templates select="Étymologie"/>
    <xsl:apply-templates select="Sous-entréesLexicales"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{entrée}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Groupes">
    <xsl:apply-templates select="Groupe"/>
</xsl:template>

<xsl:template match="Groupe">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{groupe}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="NomDeGroupe"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="PartieDuDiscours"/>
    <xsl:apply-templates select="Notes"/>
    <xsl:apply-templates select="Paradigmes"/>
    <xsl:apply-templates select="ListeDeSens"/>
    <xsl:apply-templates select="FormesGrammaticales"/>
    <xsl:apply-templates select="Étymologie"/>
    <xsl:apply-templates select="Sous-entréesLexicales"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{groupe}</xsl:text>
</xsl:template>

<xsl:template match="Sous-entréesLexicales">
    <xsl:apply-templates select="Sous-entréeLexicale"/>
</xsl:template>

<xsl:template match="Sous-entréeLexicale">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{sous-entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}[</xsl:text>
    <xsl:apply-templates select="Multimédia"/>
    <xsl:text>]</xsl:text>
    <xsl:apply-templates select="Lemme/Variantes"/>
    <xsl:apply-templates select="PartieDuDiscours"/>
    <xsl:apply-templates select="Notes"/>
    <xsl:apply-templates select="Paradigmes"/>
    <xsl:apply-templates select="ListeDeSens"/>
    <xsl:apply-templates select="FormesGrammaticales"/>
    <xsl:apply-templates select="Étymologie"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{sous-entrée}</xsl:text>
</xsl:template>

<xsl:template match="Variantes">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{variantes}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Variante">
        <xsl:apply-templates select="."/>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{variantes}</xsl:text>
</xsl:template>

<xsl:template match="Variante">
    <xsl:text>\variante{</xsl:text>
    <xsl:value-of select="replace(., '_', '\\tiretbas ')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="PartieDuDiscours">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\partiedudiscours{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Notes">
    <xsl:apply-templates select="Note"/>
</xsl:template>

<xsl:template match="Note[Type='grammaire']">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\grammaire{</xsl:text>
    <xsl:apply-templates select="Texte"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Paradigmes">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{paradigmes}</xsl:text>
    <xsl:for-each-group select="Paradigme" group-by="Catégorie">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\paradigme{</xsl:text>
        <xsl:text>\catégorieparadigmatique{</xsl:text>
        <xsl:value-of select="current-grouping-key()"/>
        <xsl:text>}}{</xsl:text>
        <xsl:for-each select="current-group()/Forme">
            <xsl:text>\formeparadigmatique{</xsl:text>
            <xsl:value-of select="replace(., '_', '\\tiretbas')"/>
            <xsl:text>}</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each-group>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{paradigmes}</xsl:text>
</xsl:template>

<xsl:template match="ListeDeSens">
    <xsl:apply-templates select="Sens"/>
</xsl:template>

<xsl:template match="Sens">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{sens}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="NuméroDeSens"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Collocation"/>
    <xsl:apply-templates select="Paradigmes"/>
    <xsl:apply-templates select="Définitions"/>
    <xsl:apply-templates select="Gloses"/>
    <xsl:apply-templates select="Usages"/>
    <xsl:apply-templates select="InformationsEncyclopédiques"/>
    <xsl:apply-templates select="NomsScientifiques"/>
    <xsl:apply-templates select="Exemples"/>
    <xsl:apply-templates select="RelationsSémantiques"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{sens}</xsl:text>
</xsl:template>

<xsl:template match="NuméroDeSens">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\numérodesens{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Définitions">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{définitions}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Définition">
        <xsl:apply-templates select="."/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{définitions}</xsl:text>
</xsl:template>

<xsl:template match="Définition">
    <xsl:text>\définition{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Gloses">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{gloses}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Glose">
        <xsl:apply-templates select="."/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{gloses}</xsl:text>
</xsl:template>

<xsl:template match="Glose">
    <xsl:text>\glose{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomsScientifiques">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{nomsscientifiques}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="NomScientifique">
        <xsl:apply-templates select="."/>
        <xsl:if test="position() != last()">
            <xsl:text> ; </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{nomsscientifiques}</xsl:text>
</xsl:template>

<xsl:template match="NomScientifique">
    <xsl:text>\nomscientifique{</xsl:text>
    <xsl:apply-templates select="Binôme"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="Famille"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="Auteur"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Binôme"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomScientifique/Binôme">
    <xsl:value-of select="replace(., ' (ssp.|subsp.|forma.|sp.|spp.|var.|cv.)', ' \\emph{$1}')"/>
</xsl:template>

<xsl:template match="NomScientifique/Auteur">
    <xsl:value-of select="replace(., '&amp;', '\\&amp;')"/>
</xsl:template>

<xsl:template match="Exemples">
    <xsl:apply-templates select="Exemple"/>
</xsl:template>

<xsl:template match="Exemple">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exemple}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="Original"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Traduction">
        <xsl:apply-templates select="."/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{exemple}</xsl:text>
</xsl:template>

<xsl:template match="InformationsEncyclopédiques">
    <xsl:apply-templates select="InformationEncyclopédique"/>
</xsl:template>

<xsl:template match="InformationEncyclopédique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{informationencyclopédique}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="Original"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Traduction">
        <xsl:apply-templates select="."/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{informationencyclopédique}</xsl:text>
</xsl:template>

<xsl:template match="Usages">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{usages}</xsl:text>
    <xsl:apply-templates select="Usage"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{usages}</xsl:text>
</xsl:template>

<xsl:template match="Usage">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{usage}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Original|Traduction">
        <xsl:apply-templates select="."/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{usage}</xsl:text>
</xsl:template>

<xsl:template match="Original">
    <xsl:text>\original{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Traduction">
    <xsl:text>\traduction{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="RelationsSémantiques">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{relationssémantiques}</xsl:text>
    <xsl:for-each-group select="RelationSémantique[not(contains(Type/text(), 'Component'))]" group-by="Type">
        <xsl:sort select="index-of(('renvoi', 'synonyme', 'antonyme'), current-grouping-key())"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\relationsémantique{</xsl:text>
        <xsl:text>\typerelationsémantique{</xsl:text>
        <xsl:call-template name="traduire">
            <xsl:with-param name="expression" select="current-grouping-key()"/>
        </xsl:call-template>
        <xsl:text>}}{</xsl:text>
        <xsl:for-each select="current-group()/Cible">
            <xsl:text>\ciblerelationsémantique{</xsl:text>
            <xsl:value-of select="replace(., '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="./@identifiant"/>
            <xsl:text>}</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>\pcmn{、}</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:for-each-group>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{relationssémantiques}</xsl:text>
</xsl:template>

<xsl:template match="Collocation">
    <xsl:apply-templates select="Composant"/>
</xsl:template>

<xsl:template match="Composant">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\composant{</xsl:text>
    <xsl:value-of select="replace(Cible, '_', '\\tiretbas ')"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="PartieDuDiscours"/>
</xsl:template>

<xsl:template match="FormesGrammaticales">
    <xsl:for-each select="FormeGrammaticale">
        <xsl:apply-templates select="."/>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
</xsl:template>

<xsl:template match="FormeGrammaticale">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\formegrammaticale{</xsl:text>
    <xsl:apply-templates select="PersonneGrammaticale"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="NombreGrammatical"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="Forme"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="PersonneGrammaticale">
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="NombreGrammatical">
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="Étymologie">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{étymologie}</xsl:text>
    <xsl:apply-templates select="Étymon"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{étymologie}</xsl:text>
</xsl:template>

<xsl:template match="Étymon">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\étymon{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Multimédia"/>

<xsl:template match="Audio"/>

<xsl:template match="lien">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\lien{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(., '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="adapter_langue">
    <xsl:text>\p</xsl:text>
    <xsl:value-of select="@langue"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="traduire">
    <xsl:param name="expression"/>
    <xsl:choose>
        <xsl:when test="$expression='renvoi'">
            <xsl:text>参考</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='synonyme'">
            <xsl:text>同义词</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='antonyme'">
            <xsl:text>反义词</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='singulier'">
            <xsl:text>s</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='pluriel'">
            <xsl:text>p</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='première'">
            <xsl:text>1</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='deuxième'">
            <xsl:text>2</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='troisième'">
            <xsl:text>3</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$expression"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="créer_expression_rationnelle_graphèmes">  <!-- forme : ^(x|y|z) -->
    <xsl:param name="graphèmes"/>
    <xsl:text>^(</xsl:text>
    <xsl:choose>
        <xsl:when test="Élément">
            <xsl:for-each select="$graphèmes/Élément">
                <xsl:value-of select="."/>
                <xsl:if test="not(position() = last())">
                    <xsl:text>|</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$graphèmes"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template name="ajouter_audio"/>

</xsl:stylesheet>
